﻿using System;
using Services;
using Strategies.Intents.Interfaces;
using Strategies.JsonObjects.Interfaces;

namespace Strategies.JsonObjects
{
    public class TransferringAudio : IStrategy
    {
        public string Type { get; set; }
        public string Data { get; set; }
        public string ID { get; set; }

        public void MakeChangesInUnity()
        {
            AudioPlayingService.Instance.CollectAudioDataBytes(Convert.FromBase64String(Data));
        }
    }
}
