import asyncio
import base64
import json
from io import BytesIO

import dialogflow_v2beta1 as dialogflow
from dialogflow_v2beta1.proto.audio_config_pb2 import SynthesizeSpeechConfig, OutputAudioConfig, VoiceSelectionParams
from dialogflow_v2beta1.proto.session_pb2 import QueryInput
from google.protobuf.json_format import MessageToJson
from pydub import AudioSegment

from constants.paths import DIALOGFLOW_OUTPUT_FOR_DEBUGGING
from constants.session_settings import *
from enums.intent_enum import Intents
from singletons.current_subject import CurrentSubject
from utilities import channel_send

output_audio_config = OutputAudioConfig(
    audio_encoding=dialogflow.enums.OutputAudioEncoding.OUTPUT_AUDIO_ENCODING_LINEAR_16,
    synthesize_speech_config=SynthesizeSpeechConfig(voice=VoiceSelectionParams(name="en-US-Wavenet-F")))

audio_config = dialogflow.types.InputAudioConfig(
    audio_encoding=dialogflow.enums.AudioEncoding.AUDIO_ENCODING_LINEAR_16, language_code=DIALOGFLOW_LANGUAGE_CODE,
    sample_rate_hertz=16000)

session_client = dialogflow.SessionsClient()
session_path = session_client.session_path(DIALOGFLOW_PROJECT_ID, SESSION_ID)


class CurrentSession:
    __instance = None

    @staticmethod
    def get_instance():
        if CurrentSession.__instance is None:
            CurrentSession()
        return CurrentSession.__instance

    def __init__(self):
        self.__channel = None
        self.__loop = None
        self.__state_context = None

        if CurrentSession.__instance is not None:
            raise Exception("This class is a singleton!")
        else:
            CurrentSession.__instance = self

    @property
    def loop(self):
        return self.__loop

    @loop.setter
    def loop(self, loop):
        self.__loop = loop

    @property
    def channel(self):
        return self.__channel

    @channel.setter
    def channel(self, channel):
        self.__channel = channel

    @property
    def state_context(self):
        return self.__state_context

    @state_context.setter
    def state_context(self, state_context):
        self.__state_context = state_context

    async def set_current_intent_and_start_dialogflow_event(self, intent):
        self.set_current_intent_variables(intent)
        await self.start_dialogflow_event(intent)

    async def start_dialogflow_event_for_current_intent(self, intent):
        await self.start_dialogflow_event(intent)

    def set_current_intent_variables(self, intent):
        CurrentSubject.get_instance().current_intent_name = intent.value["intent_name"]

    async def start_dialogflow_event(self, intent):
        event_input = dialogflow.types.EventInput(name=intent.value["intent_name"],
                                                  language_code=DIALOGFLOW_LANGUAGE_CODE)
        query_input = dialogflow.types.QueryInput(event=event_input)
        response = session_client.detect_intent(session=session_path, query_input=query_input,
                                                output_audio_config=output_audio_config)
        await self.send_dialogflow_reaction(response, intent)

    def set_dialogflow_lang_to_nl(self):
        global DIALOGFLOW_LANGUAGE_CODE, output_audio_config, audio_config
        DIALOGFLOW_LANGUAGE_CODE = "nl"
        output_audio_config = OutputAudioConfig(
            audio_encoding=dialogflow.enums.OutputAudioEncoding.OUTPUT_AUDIO_ENCODING_LINEAR_16,
            synthesize_speech_config=SynthesizeSpeechConfig(
                voice=VoiceSelectionParams(name="nl-NL-Wavenet-E")))
        audio_config = dialogflow.types.InputAudioConfig(
            audio_encoding=dialogflow.enums.AudioEncoding.AUDIO_ENCODING_LINEAR_16,
            language_code=DIALOGFLOW_LANGUAGE_CODE,
            sample_rate_hertz=16000)

    async def send_subject_audio_input_to_dialogflow_and_get_response(self):
        with open(CurrentSubject.get_instance().current_saving_path_for_audio, 'rb') as audio_file:
            input_audio = audio_file.read()
        query_input = QueryInput(audio_config=audio_config)

        response = session_client.detect_intent(
            session=session_path, query_input=query_input,
            input_audio=input_audio, output_audio_config=output_audio_config)
        print(response)
        response_json = json.loads(MessageToJson(response))

        with open(DIALOGFLOW_OUTPUT_FOR_DEBUGGING + "detected_intent_response.json", 'w') as file:
            json.dump(response_json, file, indent=4)
        if not response.query_result.query_text:
            await self.start_dialogflow_event_for_current_intent(Intents.BAD_INPUT_FALLBACK)
            response = None
        elif response.output_audio and response.query_result.fulfillment_text:
            await self.start_dialogflow_event_for_current_intent(Intents.DIALOGFLOW_DID_NOT_GOT_PARAMETER)
            response = None
        return response

    async def send_dialogflow_reaction(self, response, intent):

        response_json = json.loads(MessageToJson(response))
        with open(DIALOGFLOW_OUTPUT_FOR_DEBUGGING + "start_event_response.json", 'w') as file:
            json.dump(response_json, file, indent=4)
        wav_file = AudioSegment.from_wav(BytesIO(response.output_audio))
        wav_file.export(DIALOGFLOW_OUTPUT_FOR_DEBUGGING + "start_event_audio.wav", format="wav")

        asyncio.ensure_future(channel_send(self.__channel, json.dumps(
            {"id_sender": WEBRTC_ID, "type": "intent",
             "current_intent_name": CurrentSubject.get_instance().current_intent_name,
             "chatbot_waits_for_audio_input": intent.value["chatbot_waits_for_audio_input"],
             "fulfillment_text": response.query_result.fulfillment_text})))

        wav_file = str(base64.b64encode(wav_file.raw_data), "utf-8")

        chunks = [wav_file[i:i + 250000] for i in
                  range(0, len(wav_file), 250000)]
        for i, chunk in enumerate(chunks):
            if i == len(chunks) - 1:
                asyncio.ensure_future(
                    channel_send(self.__channel, json.dumps({"id": WEBRTC_ID, "type": "end_transferring_audio",
                                                             "data": chunk})))
            else:
                asyncio.ensure_future(
                    channel_send(self.__channel, json.dumps({"id": WEBRTC_ID, "type": "transferring_audio",
                                                             "data": chunk})))
