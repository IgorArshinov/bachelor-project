import argparse
import logging
from collections import deque
from time import perf_counter

from aiortc import (
    RTCIceCandidate,
    RTCPeerConnection,
    RTCSessionDescription,
    VideoStreamTrack, RTCDataChannel, MediaStreamTrack,
)
from aiortc.contrib.signaling import BYE, add_signaling_arguments

from states.state_context import StateContext, DisconnectedState, ConnectedState
from unity_to_python.signaler import UnityTcpSignaling
from singletons.audiovisual_data_recorder import *
from singletons.current_session import *
from singletons.current_subject import *
from strategies.json_objects.json_object_context import JsonObjectContext
from utilities import unity_input
from utilities.unity_input import channel_log

Logger.get_instance().logger.info("perf_counter() %s" % perf_counter())

starttime = time.time()
continuous_threading.set_allow_shutdown(True)
continuous_threading.set_shutdown_timeout(0)


async def run(pc, player, signaling):
    def add_tracks():
        if player and player.audio:
            pc.add_track(player.audio)

        if player and player.video:
            pc.add_track(player.video)
        else:
            pc.add_track(VideoStreamTrack())

    Logger.get_instance().logger.info("Loaded %s" % perf_counter())
    asyncio.ensure_future(CurrentSession.get_instance().state_context.transition_to(DisconnectedState()))

    @pc.on("track")
    def on_track(track: MediaStreamTrack):
        Logger.get_instance().logger.info("Receiving %s" % track.kind)

        if track.kind == "video":
            pass

        if track.kind == "audio":
            AudiovisualDataRecorder.get_instance().add_track(track)

        @track.on("ended")
        async def on_ended():
            await AudiovisualDataRecorder.get_instance().stop()
            Logger.get_instance().logger.info("Track %s ended", track.kind)

    @pc.on("iceconnectionstatechange")
    async def on_iceconnectionstatechange():
        Logger.get_instance().logger.info("ICE connection state is %s", pc.iceConnectionState)
        if pc.iceConnectionState == "failed":
            await pc.close()
        if pc.iceConnectionState == "closed":
            exit()

    @pc.on("datachannel")
    def on_datachannel(channel: RTCDataChannel):

        global current_channel
        current_channel = channel

        CurrentSession.get_instance().channel = channel

        @channel.transport.transport.on("statechange")
        async def on_close_transport():
            Logger.get_instance().logger.info("statechange")
            await pc.close()

        channel_log(channel, "-", "created by remote party")

        asyncio.ensure_future(CurrentSession.get_instance().state_context.transition_to(ConnectedState()))

        @channel.on("message")
        async def on_message(message):
            object_from_message = unity_input.process_input(message)

            if object_from_message is not None:
                Logger.get_instance().logger.info(object_from_message)
                json_object_context = JsonObjectContext.get_instance()
                json_object_context.set_strategy(object_from_message['type'])
                if "data" in object_from_message:
                    asyncio.ensure_future(json_object_context.execute_strategy_with_form_input(object_from_message))
                else:
                    asyncio.ensure_future(json_object_context.execute_strategy())

                channel_log(channel, "<", object_from_message)

    Logger.get_instance().logger.info("Waiting for signaler connection ...")
    await signaling.connect()

    while True:
        try:
            object = await signaling.receive()

            if isinstance(object, RTCSessionDescription):
                await pc.setRemoteDescription(object)

                await AudiovisualDataRecorder.get_instance().start()

                if object.type == "offer":
                    await pc.setLocalDescription(await pc.createAnswer())
                    await signaling.send(pc.localDescription)
            elif isinstance(object, RTCIceCandidate):
                pc.addIceCandidate(object)
            elif object is BYE:
                Logger.get_instance().logger.info("Exiting")
                break
        except Exception as error:
            Logger.get_instance().logger.info("Error %s", error)
            Logger.get_instance().logger.info("No connection")
            time.sleep(5)


def exit_code():
    loop.run_until_complete(AudiovisualDataRecorder.get_instance().stop())
    loop.run_until_complete(signaling.close())
    loop.run_until_complete(pc.close())


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Video stream from the command line")
    parser.add_argument("--verbose", "-v", action="count")
    parser.add_argument("--host", "-ip", help="ip address of signaler/sender instance")
    parser.add_argument("--port", "-p", help="port of signaler/sender instance")
    add_signaling_arguments(parser)
    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.WARN)
        Logger.get_instance().logger.setLevel(level=logging.INFO)

    host = args.host or "localhost"
    port = args.port or 9998

    if not os.path.exists(DIALOGFLOW_OUTPUT_FOR_DEBUGGING):
        os.makedirs(DIALOGFLOW_OUTPUT_FOR_DEBUGGING)

    if not os.path.exists("speech_detection"):
        os.makedirs("speech_detection")

    signaling = UnityTcpSignaling(host=host, port=port)
    pc = RTCPeerConnection()

    player = None
    frame_queue = deque()

    loop = asyncio.get_event_loop()
    CurrentSession.get_instance().loop = loop
    CurrentSession.get_instance().state_context = StateContext()
    filtered = AudiovisualDataRecorder.get_instance().pipeline({'audio': "speech_detection/pyannote.wav"})

    try:
        loop.run_until_complete(
            run(
                pc=pc,
                player=player,
                signaling=signaling,
            )
        )

    finally:
        Logger.get_instance().logger.info("Shutting down receiver and peer connection")
        exit_code()
