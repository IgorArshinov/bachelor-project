﻿using System;
using Objects;
using Services;
using Strategies.Intents.Interfaces;
using Strategies.JsonObjects.Interfaces;
using Utilities;

namespace Strategies.JsonObjects
{
    public class EndTransferringAudio : IStrategy
    {
        public string Type { get; set; }
        public string Data { get; set; }
        public string ID { get; set; }


        public void MakeChangesInUnity()
        {
            AudioPlayingService.Instance.CollectAudioDataBytes(Convert.FromBase64String(Data));
            AudioPlayingService.Instance.WriteAudioDataBytesFromPythonToWaveFile();
            MainThreadDispatcherScript.Instance().Enqueue(() =>
            {
                AudioPlayingService.Instance.PlayAudioFileFromPython();
            });
            AudioPlayingService.Instance.ClearAudioData();
        }
    }
}
