﻿using Objects;
using Strategies.Intents.Interfaces;
using Strategies.JsonObjects.Interfaces;

namespace Strategies.JsonObjects
{
    public class DialogState : IStrategy
    {
        public string Type { get; set; }
        public string Data { get; set; }
        public string ID { get; set; }

        public void MakeChangesInUnity()
        {
            var loadingWhenAudioIsSaving = GameObjectManagerScript.GameObjects["loadingWhenAudioIsSaving"];
            MainThreadDispatcherScript.Instance().Enqueue(() =>
            {
                if (loadingWhenAudioIsSaving.activeSelf)
                {
                    loadingWhenAudioIsSaving.SetActive(false);
                }
            });
        }
    }
}
