﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.MixedReality.WebRTC;
using Newtonsoft.Json;
using Strategies.Intents.Interfaces;
using Strategies.JsonObjects;
using Strategies.JsonObjects.Interfaces;
using UnityEngine;

namespace UnityToPython
{
    public sealed class UnityToPythonDataChannel
    {
        private UnityToPythonDataChannel()
        {
            Settings = new JsonSerializerSettings()
            {
                Converters = new List<JsonConverter>()
                {
                    new ToStrategyConverter()
                }
            };
            _strategyContext = new StrategyContext();
        }


        private static UnityToPythonDataChannel instance;
        private DataChannel _dataChannel;

        public JsonSerializerSettings Settings;
        private readonly StrategyContext _strategyContext;

        public static UnityToPythonDataChannel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new UnityToPythonDataChannel();
                }

                return instance;
            }
        }


        public DataChannel DataChannel
        {
            get => _dataChannel;
            set
            {
                _dataChannel = value;
                _dataChannel.MessageReceived += Channel_MessageReceived;

            }
        }

        private void Channel_MessageReceived(byte[] message)
        {
            Debug.Log("message: " + message);
            var stringFromMessage = Encoding.ASCII.GetString(message);
            Debug.Log("stringFromMessage: " + stringFromMessage);

            IStrategy strategyFromMessage = null;
            try
            {
                strategyFromMessage = JsonConvert.DeserializeObject<IStrategy>(stringFromMessage, Settings);
            }
            catch (Exception exception)
            {
                Debug.Log(exception);
            }


            if (strategyFromMessage != null)
            {
                _strategyContext.SetStrategy(strategyFromMessage);
                _strategyContext.ExecuteStrategy();
            }
        }

        public void SendMessage(string message)
        {
            if (_dataChannel.State.Equals(DataChannel.ChannelState.Open))
            {
                byte[] chatMessage = Encoding.ASCII.GetBytes(message);
                _dataChannel.SendMessage(chatMessage);
            }
            else
            {
                Debug.Log("Channel is not open");
            }
        }
    }
}
