from abc import abstractmethod, ABC
from enum import Enum

from singletons.current_session import CurrentSession
from singletons.current_subject import CurrentSubject
from singletons.logger import Logger
from states import DialogState, VideoIsPlayingState, VideoIsStoppedState, \
    ChatbotWaitsForAudioInputState
from strategies.intents.intent_context import IntentContext


class JsonObjectEnum(Enum):
    PLAY = "play"
    STOP = "stop"
    INPUT_FORM = "input_form"
    VIDEO_IS_PLAYING = "video_is_playing"
    VIDEO_IS_STOPPED = "video_is_stopped"
    CHATBOT_WAITS_FOR_AUDIO_INPUT = "chatbot_waits_for_audio_input"
    TASK = "task"


class JsonObjectContext:
    __instance = None

    @staticmethod
    def get_instance():
        if JsonObjectContext.__instance is None:
            JsonObjectContext()
        return JsonObjectContext.__instance

    def __init__(self):
        self.__strategy = None
        self.__json_objects = {
            JsonObjectEnum.PLAY.value: Play(),
            JsonObjectEnum.STOP.value: Stop(),
            JsonObjectEnum.INPUT_FORM.value: InputForm(),
            JsonObjectEnum.VIDEO_IS_PLAYING.value: VideoIsPlaying(),
            JsonObjectEnum.VIDEO_IS_STOPPED.value: VideoIsStopped(),
            JsonObjectEnum.CHATBOT_WAITS_FOR_AUDIO_INPUT.value: ChatbotWaitsForAudioInput(),
            JsonObjectEnum.TASK.value: Task(),
        }
        if JsonObjectContext.__instance is not None:
            raise Exception("This class is a singleton!")
        else:
            JsonObjectContext.__instance = self

    def set_strategy(self, strategy):
        self.__strategy = self.__json_objects[strategy]

    async def execute_strategy(self):
        await self.__strategy.make_changes_in_python()

    async def execute_strategy_with_form_input(self, object_from_message):
        await self.__strategy.make_changes_in_python_with_input_form(object_from_message)


class JsonObjectStrategy(ABC):
    pass


class JsonObjectStrategyWithoutFormInput(JsonObjectStrategy):
    @abstractmethod
    def make_changes_in_python(self):
        pass


class JsonObjectStrategyWithFormInput(JsonObjectStrategy):
    @abstractmethod
    def make_changes_in_python_with_input_form(self, object_from_message):
        pass


class Stop(JsonObjectStrategyWithFormInput):
    async def make_changes_in_python_with_input_form(self, object_from_message):
        Logger.get_instance().logger.info(object_from_message)


class VideoIsPlaying(JsonObjectStrategyWithoutFormInput):
    async def make_changes_in_python(self):
        await CurrentSession.get_instance().state_context.transition_to(VideoIsPlayingState())


class VideoIsStopped(JsonObjectStrategyWithoutFormInput):
    async def make_changes_in_python(self):
        await CurrentSession.get_instance().state_context.transition_to(VideoIsStoppedState())


class ChatbotWaitsForAudioInput(JsonObjectStrategyWithoutFormInput):
    async def make_changes_in_python(self):
        await CurrentSession.get_instance().state_context.transition_to(ChatbotWaitsForAudioInputState())


class Play(JsonObjectStrategyWithoutFormInput):

    async def make_changes_in_python(self):
        await CurrentSession.get_instance().state_context.transition_to(DialogState())


class InputForm(JsonObjectStrategyWithFormInput):
    async def make_changes_in_python_with_input_form(self, object_from_message):
        IntentContext.get_instance().set_strategy(CurrentSubject.get_instance().current_intent_name)
        await IntentContext.get_instance().execute_strategy_with_form_input(object_from_message)


class Task(JsonObjectStrategyWithoutFormInput):
    async def make_changes_in_python(self):
        IntentContext.get_instance().set_strategy(CurrentSubject.get_instance().current_intent_name)
        await IntentContext.get_instance().execute_strategy()
