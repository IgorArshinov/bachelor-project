import os
from pathlib import Path

ROOT_DIR = os.path.dirname(os.path.abspath(Path(__file__).parent))
COLLECTED_DATA_PATH = ROOT_DIR + "/collected_data/"
DIALOGFLOW_OUTPUT_FOR_DEBUGGING = ROOT_DIR + "/dialogflow_output/"
WAV_FILE_TYPE = ".wav"
MP4_FILE_TYPE = ".mp4"
