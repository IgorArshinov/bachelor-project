﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Strategies;
using Strategies.Intents.Interfaces;
using Strategies.JsonObjects;
using Strategies.JsonObjects.Interfaces;

namespace UnityToPython
{
    public class ToStrategyConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(IStrategy);
        }

        public override object ReadJson(JsonReader reader,
            Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject jObject = JObject.Load(reader);
            var type = jObject["type"].Value<string>();
            switch (type)
            {
                case "intent":
                    return jObject.ToObject<Intent>();
                case "transferring_audio":
                    return jObject.ToObject<TransferringAudio>();
                case "end_transferring_audio":
                    return jObject.ToObject<EndTransferringAudio>();
                case "connected_state":
                    return jObject.ToObject<ConnectedState>();
                case "dialog_state":
                    return jObject.ToObject<DialogState>();
                case "audio_saving_state":
                    return jObject.ToObject<AudioSavingState>();
                default:
                    return null;
            }
        }

        public override void WriteJson(JsonWriter writer,
            object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
