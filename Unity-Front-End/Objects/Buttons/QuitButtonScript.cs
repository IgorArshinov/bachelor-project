﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityToPython;

namespace Objects.Buttons
{
    public class QuitButtonScript : MonoBehaviour
    {
        private Button Button { get; set; }

        private void Awake()
        {
            Button = gameObject.GetComponent<Button>();
            Button.onClick.AddListener(OnClick);
        }

        private void OnClick()
        {
            try
            {
                RtcServer.Instance.Signaler.PeerConnection.Dispose();
                RtcServer.Instance.Signaler.Stop();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }

#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
                     Application.Quit();
#endif
        }

        private void Start()
        {
        }

        private void Update()
        {
        }
    }
}
