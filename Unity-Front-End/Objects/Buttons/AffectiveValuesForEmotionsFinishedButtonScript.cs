﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Strategies;
using Strategies.Intents;
using UnityEngine;
using UnityEngine.UI;
using UnityToPython;

namespace Objects
{
    public class AffectiveValuesForEmotionsFinishedButtonScript : MonoBehaviour
    {
        public static Button Button { get; private set; }
        [SerializeField] private string[] emotionLabels;
        [SerializeField] private Slider[] arousalSliders;
        [SerializeField] private Slider[] valenceSliders;

        private void Awake()
        {
            Button = gameObject.GetComponent<Button>();
            Button.onClick.AddListener(OnClick);
        }

        private void Update()
        {
        }

        private void OnClick()
        {
            IDictionary<string, object> values = new Dictionary<string, object>();
            for (int i = 0; i < emotionLabels.Length; i++)
            {
                values.Add(emotionLabels[i], new
                {
                    arousal_value = arousalSliders[i].value / 100,
                    valence_value = valenceSliders[i].value / 100
                });
            }


            var formData = new
            {
                form_name = "affective_values_for_emotions",
                form_data = values
            };


            if (RtcServer.Instance.Signaler.PeerConnection.IsConnected)
            {
                var jsonObject = JsonConvert.SerializeObject(new
                {
                    id = "Unity_client", type = "input_form",
                    data = formData,
                    current_intent_name = IntentContext.Instance.Name
                });
                UnityToPythonDataChannel.Instance.SendMessage(jsonObject);
            }
        }
    }
}
