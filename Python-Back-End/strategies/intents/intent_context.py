import os
import shutil
from abc import abstractmethod, ABC

from constants.paths import COLLECTED_DATA_PATH
from enums.intent_enum import Intents

from singletons.current_session import CurrentSession
from singletons.current_subject import CurrentSubject
from singletons.logger import Logger
from utilities import write_to_json_file


class IntentContext:
    __instance = None

    @staticmethod
    def get_instance():
        if IntentContext.__instance is None:
            IntentContext()
        return IntentContext.__instance

    def __init__(self):
        self.__strategy = None
        self.__intents = {
            Intents.INTRODUCTION.value["intent_name"]: IntroductionIntent(Intents.SMALL_TALK_ABOUT_LANGUAGE),
            Intents.SMALL_TALK_ABOUT_LANGUAGE.value["intent_name"]: IntentWithoutFollowUpAndWithUserAudioInput(
                Intents.DESCRIBE_YOUR_EMOTION_IN_ONE_WORD),
            Intents.DESCRIBE_YOUR_EMOTION_IN_ONE_WORD.value["intent_name"]: DescribeYourEmotionInOneWordIntent(
                Intents.SELECT_AFFECTIVE_SLIDER_VALUES),
            Intents.SELECT_AFFECTIVE_SLIDER_VALUES.value["intent_name"]: SelectAffectiveSliderValuesIntent(
                Intents.SMALL_TALK_ABOUT_EMOTIONS_AND_QUESTION_ABOUT_ROBOT_EMOTIONS),
            Intents.SMALL_TALK_ABOUT_EMOTIONS_AND_QUESTION_ABOUT_ROBOT_EMOTIONS.value[
                "intent_name"]: IntentWithoutFollowUpAndWithUserAudioInput(
                Intents.SMALL_TALK_ABOUT_FAVORITE_ARTISTS),
            Intents.SMALL_TALK_ABOUT_FAVORITE_ARTISTS.value["intent_name"]: IntentWithoutFollowUpAndWithUserAudioInput(
                Intents.FORM_FOR_METADATA),
            Intents.FORM_FOR_METADATA.value["intent_name"]: FormForMetadataIntent(
                Intents.FORM_FOR_AFFECTIVE_VALUES_FOR_EMOTIONS),
            Intents.FORM_FOR_AFFECTIVE_VALUES_FOR_EMOTIONS.value[
                "intent_name"]: FormForAffectiveValuesForEmotionsIntent(
                Intents.EXPLANATION_ABOUT_TASKS),
            Intents.EXPLANATION_ABOUT_TASKS.value["intent_name"]: ExplanationAboutTasksIntent(),
            Intents.TASK_QUESTION_SADNESS.value["intent_name"]: IntentWithTask(
                Intents.DESCRIBE_YOUR_EMOTION_IN_MULTIPLE_SENTENCES_AFTER_TASK),
            Intents.TASK_QUESTION_FATIGUE.value["intent_name"]: IntentWithTask(
                Intents.DESCRIBE_YOUR_EMOTION_IN_MULTIPLE_SENTENCES_AFTER_TASK),
            Intents.TASK_QUESTION_JOVIALITY.value["intent_name"]: IntentWithTask(
                Intents.DESCRIBE_YOUR_EMOTION_IN_MULTIPLE_SENTENCES_AFTER_TASK),
            Intents.TASK_QUESTION_ATTENTIVENESS.value["intent_name"]: IntentWithTask(
                Intents.DESCRIBE_YOUR_EMOTION_IN_MULTIPLE_SENTENCES_AFTER_TASK),
            Intents.DESCRIBE_YOUR_EMOTION_IN_MULTIPLE_SENTENCES_AFTER_TASK.value[
                "intent_name"]: IntentWithoutFollowUpAfterTask(
                Intents.DESCRIBE_YOUR_EMOTION_IN_ONE_WORD_AFTER_TASK),
            Intents.DESCRIBE_YOUR_EMOTION_IN_ONE_WORD_AFTER_TASK.value[
                "intent_name"]: DescribeYourEmotionInOneWordAfterTaskIntent(
                Intents.SELECT_AFFECTIVE_SLIDER_VALUES_AFTER_TASK),
            Intents.SELECT_AFFECTIVE_SLIDER_VALUES_AFTER_TASK.value[
                "intent_name"]: SelectAffectiveSliderValuesAfterTaskIntent(),
            Intents.END_SESSION.value["intent_name"]: EndSessionIntent()
        }
        if IntentContext.__instance is not None:
            raise Exception("This class is a singleton!")
        else:
            IntentContext.__instance = self

    def set_strategy(self, strategy):
        self.__strategy = self.__intents[strategy]

    async def execute_strategy(self):
        Logger.get_instance().logger.info("forms: %s", CurrentSubject.get_instance().forms)
        Logger.get_instance().logger.info("audios: %s", CurrentSubject.get_instance().audios)
        Logger.get_instance().logger.info("audios_from_tasks: %s", CurrentSubject.get_instance().audios_from_tasks)
        Logger.get_instance().logger.info("distances: %s", CurrentSubject.get_instance().distances)
        Logger.get_instance().logger.info("current_valence: %s", CurrentSubject.get_instance().current_valence)
        Logger.get_instance().logger.info("current_arousal: %s", CurrentSubject.get_instance().current_arousal)
        Logger.get_instance().logger.info("current_label_for_emotion: %s",
                                          CurrentSubject.get_instance().current_label_for_emotion)
        Logger.get_instance().logger.info("current_saving_path_for_audio: %s",
                                          CurrentSubject.get_instance().current_saving_path_for_audio)

        await self.__strategy.make_changes_in_python()

    async def execute_strategy_with_form_input(self, object_from_message):
        Logger.get_instance().logger.info("forms: %s", CurrentSubject.get_instance().forms)
        Logger.get_instance().logger.info("audios: %s", CurrentSubject.get_instance().audios)
        Logger.get_instance().logger.info("audios_from_tasks: %s", CurrentSubject.get_instance().audios_from_tasks)
        Logger.get_instance().logger.info("distances: %s", CurrentSubject.get_instance().distances)
        Logger.get_instance().logger.info("current_valence: %s", CurrentSubject.get_instance().current_valence)
        Logger.get_instance().logger.info("current_arousal: %s", CurrentSubject.get_instance().current_arousal)
        Logger.get_instance().logger.info("current_label_for_emotion: %s",
                                          CurrentSubject.get_instance().current_label_for_emotion)
        Logger.get_instance().logger.info("current_saving_path_for_audio: %s",
                                          CurrentSubject.get_instance().current_saving_path_for_audio)

        await self.__strategy.make_changes_in_python_with_input_form(object_from_message)


class IntentStrategy(ABC):
    pass


class IntentStrategyWithoutFormInput(IntentStrategy):
    @abstractmethod
    def make_changes_in_python(self):
        pass


class IntentStrategyWithFormInput(IntentStrategy):
    @abstractmethod
    def make_changes_in_python_with_input_form(self, object_from_message):
        pass


class DescribeYourEmotionInOneWordIntent(IntentStrategyWithFormInput):
    def __init__(self, intent):
        self.__next_event = intent

    async def make_changes_in_python_with_input_form(self, object_from_message):
        CurrentSubject.get_instance().current_label_for_emotion = object_from_message['data']

        set_emotion_label_for_saved_audio_data()

        await CurrentSession.get_instance().set_current_intent_and_start_dialogflow_event(
            self.__next_event)


class SelectAffectiveSliderValuesIntent(IntentStrategyWithFormInput):
    def __init__(self, intent):
        self.__next_event = intent

    async def make_changes_in_python_with_input_form(self, object_from_message):
        CurrentSubject.get_instance().current_arousal = object_from_message["data"]["arousal"]
        CurrentSubject.get_instance().current_valence = object_from_message["data"]["valence"]

        set_affective_values_for_saved_audio_data()

        await CurrentSession.get_instance().set_current_intent_and_start_dialogflow_event(
            self.__next_event)


class FormForMetadataIntent(IntentStrategyWithFormInput):
    def __init__(self, intent):
        self.__next_event = intent

    async def make_changes_in_python_with_input_form(self, object_from_message):
        form_data = object_from_message["data"]["form_data"]
        name = form_data["name"]
        surname = form_data["surname"]
        path_to_save_to = COLLECTED_DATA_PATH + name + "_" + surname

        if os.path.exists(path_to_save_to):
            path_exists = True
            counter = 1
            while path_exists:
                counter += 1
                CurrentSubject.get_instance().session_number = "session_" + str(counter)
                temp_path_to_save_to = path_to_save_to + "/" + CurrentSubject.get_instance().session_number
                path_exists = os.path.exists(
                    temp_path_to_save_to)
                if path_exists is False:
                    path_to_save_to = temp_path_to_save_to
            try:
                os.replace(
                    COLLECTED_DATA_PATH + CurrentSubject.get_instance().full_name + "/session_" + str(1),
                    path_to_save_to)
                os.chmod(path_to_save_to, 0o777)
                shutil.rmtree(COLLECTED_DATA_PATH + CurrentSubject.get_instance().full_name)
            except Exception as exception:
                Logger.get_instance().logger.info("FormForMetadataIntent: %s", exception)
        else:
            try:
                print(path_to_save_to)
                os.replace(COLLECTED_DATA_PATH + CurrentSubject.get_instance().full_name,
                           path_to_save_to)
                os.chmod(path_to_save_to, 0o777)
            except Exception as exception:
                Logger.get_instance().logger.info("FormForMetadataIntent: %s", exception)

        for audio in CurrentSubject.get_instance().audios:
            audio["path"] = audio["path"].replace(CurrentSubject.get_instance().full_name,
                                                  name + "_" + surname)
            if CurrentSubject.get_instance().session_number.endswith("1") is False:
                audio["path"] = audio["path"].replace("session_1",
                                                      CurrentSubject.get_instance().session_number)

        CurrentSubject.get_instance().forms["metadata"] = form_data
        CurrentSubject.get_instance().name = name
        CurrentSubject.get_instance().surname = surname

        await CurrentSession.get_instance().set_current_intent_and_start_dialogflow_event(
            self.__next_event)


class FormForAffectiveValuesForEmotionsIntent(IntentStrategyWithFormInput):
    def __init__(self, intent):
        self.__next_event = intent

    async def make_changes_in_python_with_input_form(self, object_from_message):
        CurrentSubject.get_instance().forms["affective_values_for_emotions"] = object_from_message[
            "data"]["form_data"]

        await CurrentSession.get_instance().set_current_intent_and_start_dialogflow_event(
            self.__next_event)


class IntentWithoutFollowUpAfterTask(IntentStrategyWithoutFormInput):
    def __init__(self, intent):
        self.__next_event = intent

    async def make_changes_in_python(self):
        add_audio_after_task_and_set_label_and_affective_values()

        await CurrentSession.get_instance().set_current_intent_and_start_dialogflow_event(
            self.__next_event)


class EndSessionIntent(IntentStrategyWithFormInput):
    async def make_changes_in_python_with_input_form(self, object_from_message):
        CurrentSubject.get_instance().forms.update(object_from_message['data'])
        CurrentSubject.get_instance().is_doing_tasks = False
        path_to_subject_json_data_directory = CurrentSubject.get_instance().get_path_to_subject_json_data_directory()
        write_to_json_file(CurrentSubject.get_instance().forms, path_to_subject_json_data_directory + "forms_data.json")
        write_to_json_file(CurrentSubject.get_instance().audios,
                           path_to_subject_json_data_directory + "audios_data.json")
        write_to_json_file(CurrentSubject.get_instance().audios_from_tasks,
                           path_to_subject_json_data_directory + "audios_from_tasks_data.json")


class SelectAffectiveSliderValuesAfterTaskIntent(IntentStrategyWithFormInput):
    async def make_changes_in_python_with_input_form(self, object_from_message):
        current_subject = CurrentSubject.get_instance()
        current_subject.current_valence = object_from_message["data"]["valence"]
        current_subject.current_arousal = object_from_message["data"]["arousal"]

        set_affective_values_for_saved_task_audio_data()

        current_subject.set_current_task_as_finished()
        next_task_intent = current_subject.get_next_task_intent_for_subject()

        current_subject.reset_label_and_affective_values()
        if next_task_intent is not None:
            await CurrentSession.get_instance().set_current_intent_and_start_dialogflow_event(
                next_task_intent)
        else:
            await CurrentSession.get_instance().set_current_intent_and_start_dialogflow_event(
                Intents.END_SESSION)
            from singletons.audiovisual_data_recorder import AudiovisualDataRecorder
            await AudiovisualDataRecorder.get_instance().stop()


class DescribeYourEmotionInOneWordAfterTaskIntent(IntentStrategyWithFormInput):
    def __init__(self, intent):
        self.__next_event = intent

    async def make_changes_in_python_with_input_form(self, object_from_message):
        CurrentSubject.get_instance().current_label_for_emotion = object_from_message['data']

        set_emotion_label_for_saved_task_audio_data()

        await CurrentSession.get_instance().set_current_intent_and_start_dialogflow_event(
            self.__next_event)


class IntentWithoutFollowUpAndWithUserAudioInput(IntentStrategyWithoutFormInput):
    def __init__(self, intent):
        self.__next_event = intent

    async def make_changes_in_python(self):
        add_audio_and_set_label_and_affective_values()
        await CurrentSession.get_instance().set_current_intent_and_start_dialogflow_event(
            self.__next_event)


class IntentWithTask(IntentStrategyWithoutFormInput):
    def __init__(self, intent):
        self.__next_event = intent

    async def make_changes_in_python(self):
        # CurrentSubject.get_instance().chatbot_waits_for_audio_input = False
        await CurrentSession.get_instance().set_current_intent_and_start_dialogflow_event(
            self.__next_event)


class ExplanationAboutTasksIntent(IntentStrategyWithFormInput):

    async def make_changes_in_python_with_input_form(self, object_from_message):
        # CurrentSubject.get_instance().current_stage = "doing_tasks"
        next_task_intent = CurrentSubject.get_instance().get_next_task_intent_for_subject()

        CurrentSubject.get_instance().reset_label_and_affective_values()
        CurrentSubject.get_instance().is_doing_tasks = True
        await CurrentSession.get_instance().set_current_intent_and_start_dialogflow_event(
            next_task_intent)


class IntentWithFollowUp(IntentStrategyWithoutFormInput):
    def __init__(self, intent):
        self.__next_event = intent

    async def make_changes_in_python(self):
        Logger.get_instance().logger.info("make_changes_in_python")
        response = await CurrentSession.get_instance().send_subject_audio_input_to_dialogflow_and_get_response()
        if response and response.query_result.parameters.fields['languages'].string_value in (
                'NL', 'Dutch', 'Nederlands', 'Ned'):
            CurrentSession.get_instance().set_dialogflow_lang_to_nl()
            await CurrentSession.get_instance().set_current_intent_and_start_dialogflow_event(
                self.__next_event)


class IntroductionIntent(IntentStrategyWithFormInput):
    def __init__(self, intent):
        self.__next_event = intent

    async def make_changes_in_python_with_input_form(self, object_from_message):
        if object_from_message["data"]["form_data"] == 'Dutch':
            CurrentSession.get_instance().set_dialogflow_lang_to_nl()

        CurrentSubject.get_instance().forms["language_selection"] = object_from_message["data"]["form_data"]
        await CurrentSession.get_instance().set_current_intent_and_start_dialogflow_event(
            self.__next_event)


def add_audio_and_set_label_and_affective_values():
    print(CurrentSubject.get_instance().current_saving_path_for_audio)
    CurrentSubject.get_instance().audios.append(
        {"id": CurrentSubject.get_instance().current_intent_name,
         "path": CurrentSubject.get_instance().current_saving_path_for_audio,
         "emotion_label": CurrentSubject.get_instance().current_label_for_emotion,
         "affective_values": {"arousal_value": CurrentSubject.get_instance().current_arousal,
                              "valence_value": CurrentSubject.get_instance().current_valence}})


def add_audio_after_task_and_set_label_and_affective_values():
    audios_from_task = next(task for task in CurrentSubject.get_instance().audios_from_tasks if
                            task["id"] == CurrentSubject.get_instance().current_task_emotion)
    audios_from_task["audios"].append({"id": CurrentSubject.get_instance().current_intent_name,
                                       "path": CurrentSubject.get_instance().current_saving_path_for_audio,
                                       "emotion_label": CurrentSubject.get_instance().current_label_for_emotion,
                                       "affective_values": {
                                           "arousal_value": CurrentSubject.get_instance().current_arousal,
                                           "valence_value": CurrentSubject.get_instance().current_valence}})


def set_affective_values_for_saved_audio_data():
    for audio in CurrentSubject.get_instance().audios:
        audio["affective_values"] = {
            "arousal_value": CurrentSubject.get_instance().current_arousal,
            "valence_value": CurrentSubject.get_instance().current_valence}


def set_emotion_label_for_saved_audio_data():
    for audio in CurrentSubject.get_instance().audios:
        audio["emotion_label"] = CurrentSubject.get_instance().current_label_for_emotion


def set_affective_values_for_saved_task_audio_data():
    audios_from_task = next(task for task in CurrentSubject.get_instance().audios_from_tasks if
                            task["id"] == CurrentSubject.get_instance().current_task_emotion)
    for audio in audios_from_task["audios"]:
        if audio["id"].startswith("Task_Question_") is False:
            audio["affective_values"] = {
                "arousal_value": CurrentSubject.get_instance().current_arousal,
                "valence_value": CurrentSubject.get_instance().current_valence}


def set_emotion_label_for_saved_task_audio_data():
    audios_from_task = next(task for task in CurrentSubject.get_instance().audios_from_tasks if
                            task["id"] == CurrentSubject.get_instance().current_task_emotion)
    for audio in audios_from_task["audios"]:
        if audio["id"].startswith("Task_Question_") is False:
            audio["emotion_label"] = CurrentSubject.get_instance().current_label_for_emotion
