# Bachelor-Project

During my internship at the e-Media research lab of KU Leuven I developed a real-time system that could be used to collect emotional audio data for the existing machine learning model. That model was used for speech emotion recognition. I worked on the system alone and I could get help from my mentors when I needed them.

The front end uses a chatbot that can communicate with test subjects and make them emotional. The chatbot also asks the test subjects to label their emotions. The labeled audio data can then be used to update the machine learning model. To make the test subjects more emotional the chatbot gives them tasks. For example, the chatbot asks them to act the emotion joviality after showing a video clip with an actor who is jovial. When all the tasks are completed the chatbot asks the test subjects to evaluate the usability of the system. 

The back end saves the collected labeled data on a hard drive. In the folder collected_data in the folder Python-Back-End you can find which data is being collected with this system. The back end can also be used to recognize the acted emotions. 

For the front end I used the game engine Unity. For the back end I used Python, because the model was also developed with Python. In the project folders you can find which packages I used for the system and the code that I wrote. To develop the system, I used three design patterns: the strategy pattern, the state pattern and the singleton pattern. 

The whole system can be run on the test subject's pc. To run the system a computer is required to have a graphics card and an internet connection. To create the executable for the back end I used PyInstaller. The system was only tested on Windows. 

The presentation in this folder is in Dutch and has embedded videos to demonstrate how the system works. The research document in this folder is also written in Dutch. In the research document you can find info about the internship project, research questions, research method and my conclusions.





