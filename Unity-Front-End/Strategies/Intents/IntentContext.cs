﻿using System.Collections.Generic;
using Enums;
using Strategies.Intents.Interfaces;

namespace Strategies.Intents
{
    public sealed class IntentContext
    {
        private static IntentContext instance;
        private IIntentStrategy _strategy;

        private readonly Dictionary<string, IIntentStrategy> _intents = new Dictionary<string, IIntentStrategy>()
        {
            {
                IntentEnum.Introduction, new IntentWithPanel(IntentEnum.Introduction)
            },
            { IntentEnum.SmallTalkAboutLanguage, new IntentWithoutPanel() },
            {
                IntentEnum.SmallTalkAboutEmotionsAndQuestionAboutRobotEmotions,
                new IntentWithoutPanel()
            },
            {
                IntentEnum.SmallTalkAboutFavoriteArtists,
                new IntentWithoutPanel()
            },
            {
                IntentEnum.DescribeYourEmotionInMultipleSentencesAfterTask,
                new IntentWithoutPanel()
            },
            { IntentEnum.DescribeYourEmotionInOneWord, new IntentWithPanel(IntentEnum.DescribeYourEmotionInOneWord) },
            { IntentEnum.SelectAffectiveSliderValues, new IntentWithPanel(IntentEnum.SelectAffectiveSliderValues) },
            { IntentEnum.FormForMetadata, new IntentWithPanel(IntentEnum.FormForMetadata) },
            {
                IntentEnum.FormForAffectiveValuesForEmotions,
                new IntentWithPanel(IntentEnum.FormForAffectiveValuesForEmotions)
            },
            { IntentEnum.ExplanationAboutTasks, new IntentWithPanel(IntentEnum.ExplanationAboutTasks) },
            { IntentEnum.TaskQuestionSadness, new IntentWithTask(IntentEnum.TaskQuestionSadness, "Sadness") },
            { IntentEnum.TaskQuestionFatigue, new IntentWithTask(IntentEnum.TaskQuestionFatigue, "Fatigue") },
            {
                IntentEnum.TaskQuestionAttentiveness,
                new IntentWithTask(IntentEnum.TaskQuestionAttentiveness, "Attentiveness")
            },
            { IntentEnum.TaskQuestionJoviality, new IntentWithTask(IntentEnum.TaskQuestionJoviality, "Joviality") },
            {
                IntentEnum.DescribeYourEmotionInOneWordAfterTask,
                new IntentWithPanel(IntentEnum.DescribeYourEmotionInOneWordAfterTask)
            },
            {
                IntentEnum.SelectAffectiveSliderValuesAfterTask,
                new IntentWithPanel(IntentEnum.SelectAffectiveSliderValues)
            },
            {
                IntentEnum.EndSession,
                new IntentWithPanel(IntentEnum.EndSession)
            }
        };


        private IntentContext()
        {
        }

        public static IntentContext Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new IntentContext();
                }

                return instance;
            }
        }

        public string Name { get; set; }

        public bool ChatbotWaitsForAudioInput { get; set; }

        public void SetStrategy(string strategy)
        {
            _strategy = _intents[strategy];
        }

        public void ExecuteStrategy(string fulfillmentText)
        {
            _strategy.MakeChangesInUnity(fulfillmentText);
        }
    }
}
