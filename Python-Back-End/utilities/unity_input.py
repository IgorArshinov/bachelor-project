import json


def process_input(message: bytes) -> str:
    string_from_message = str(message, 'utf-8')
    try:
        return json.loads(string_from_message)
    except Exception as exception:
        print(exception)


def rms_normalization(sound, target_dbfs):
    change_in_dbfs = target_dbfs - sound.dBFS
    return sound.apply_gain(change_in_dbfs)


async def channel_send(channel, message):
    channel_log(channel, ">", message)
    channel.send(message)


def channel_log(channel, t, message):
    print("channel(%s) %s %s" % (channel.label, t, message))
