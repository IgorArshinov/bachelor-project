using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.MixedReality.WebRTC;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace UnityToPython
{
    public abstract class Signaler
    {
        private PeerConnection _peerConnection;

        public PeerConnection PeerConnection
        {
            get => _peerConnection;
        }

        /// <summary>
        /// Event invoked when an ICE candidate message has been received from the remote peer's signaler.
        /// </summary>
        public PeerConnection.IceCandidateReadytoSendDelegate IceCandidateReceived;

        /// <summary>
        /// Event invoked when an SDP offer or answer message has been received from the remote peer's signaler.
        /// </summary>
        public PeerConnection.LocalSdpReadyToSendDelegate SdpMessageReceived;

        public System.Action ClientConnected;
        public System.Action ClientDisconnected;
        public readonly List<IceServer> IceServers = new List<IceServer> { };
        public bool PeerConnectionIsConnected;

        public abstract void SendMessage(JObject json);

        public abstract void Start();
        public abstract void Stop();


        public Signaler()
        {
            SdpMessageReceived += ProcessSdpMessage;
            ClientConnected += SetUpPeer;
            ClientDisconnected += TearDownPeer;
        }

        private async void SetUpPeer()
        {
            _peerConnection = new PeerConnection
            {
                Name = "Unity",
                PreferredVideoCodec = "H264",
            };

            var config = new PeerConnectionConfiguration
            {
                IceServers = IceServers
            };

            await _peerConnection.InitializeAsync(config);
            PeerConnection.LocalSdpReadytoSend += PeerConnection_LocalSdpReadytoSend;
            PeerConnection.IceCandidateReadytoSend += PeerConnection_IceCandidateReadytoSend;
            PeerConnection.Connected += PeerConnection_Connected;
            PeerConnection.IceStateChanged += (IceConnectionState newState) =>
            {
                Debug.Log($"ICE state: {newState}");
                if (newState.Equals(IceConnectionState.Disconnected) || newState.Equals(IceConnectionState.Failed))
                {
                    // we cannot dispose peer connection from WITHIN a callback
                    // start thread and to it later (by emitting ClientDisconnected event)
                    Task.Factory.StartNew(async () =>
                    {
                        await Task.Delay(100);
                        ClientDisconnected?.Invoke();
                    });
                }
            };
            UnityToPythonDataChannel.Instance.DataChannel =
                await PeerConnection.AddDataChannelAsync("unity-to-python-data-channel", true, true);
            Debug.Log("PeerConnection.DataChannels: " + PeerConnection.DataChannels.Count);
        }
        
        private void PeerConnection_Connected()
        {
            Debug.Log("PeerConnection: connected.");
            PeerConnectionIsConnected = true;
        }

        private void TearDownPeer()
        {
            Debug.Log("Dispose PeerConnection.");
            _peerConnection?.Dispose();
        }

        private void PeerConnection_IceCandidateReadytoSend(IceCandidate candidate)
        {
            // See ProcessIncomingMessages() for the message format


            JObject iceCandidate = new JObject
            {
                {"type", "ice"},
                {"candidate", candidate.Content},
                {"sdpMLineindex", candidate.SdpMlineIndex},
                {"sdpMid", candidate.SdpMid}
            };
            
            SendMessage(iceCandidate);
        }

        private void PeerConnection_LocalSdpReadytoSend(SdpMessage message)
        {
            // See ProcessIncomingMessages() for the message format
            string typeStr = SdpMessage.TypeToString(message.Type);

            // https://github.com/microsoft/MixedReality-WebRTC/issues/501#issuecomment-674469381
            // sdp message headeer is deprecated and needs fixing
            JObject sdpMessage = new JObject
            {
                {"type", "sdp"},
                {
                    typeStr,
                    message.Content.Replace("msid-semantic: WMS\r", "msid-semantic: WMS local_av_stream\r")
                        .Replace("msid:-", "msid:-local_av_stream")
                }
            };
            SendMessage(sdpMessage);
        }


        protected void ProcessIncomingMessage(JObject json)
        {
            if ((string) json["type"] == "ice")
            {
                string sdpMid = json["sdpMid"].Value<string>();
                int sdpMlineindex = json["sdpMLineindex"].Value<int>();
                string candidate = json["candidate"].Value<string>();
                var iceCandidate = new IceCandidate
                {
                    SdpMid = sdpMid,
                    SdpMlineIndex = sdpMlineindex,
                    Content = candidate
                };
                PeerConnection.AddIceCandidate(iceCandidate);
            }

            if ((string) json["type"] == "sdp")
            {
                string sdp;
                string type;
                if (json.ContainsKey("offer"))
                {
                    sdp = json["offer"].Value<string>();
                    type = "offer";
                }
                else
                {
                    type = "answer";
                    sdp = (string) json["answer"];
                }

                Debug.Log($"[<-] SDP message: {type} {sdp}");
                var message = new SdpMessage {Type = SdpMessage.StringToType(type), Content = sdp};
                SdpMessageReceived?.Invoke(message);
            }
        }

        private async void ProcessSdpMessage(SdpMessage message)
        {
            await PeerConnection.SetRemoteDescriptionAsync(message);

            if (message.Type == SdpMessageType.Offer)
            {
                PeerConnection.CreateAnswer();
            }
        }

        private string EnsureSemanticGrouping(string sdpMessage)
        {
            //Checks if the msid semantic group has been set in the message.
            if (!sdpMessage.Contains("msid-semantic: WMS\r"))
                return sdpMessage;

            return sdpMessage.Replace("msid-semantic: WMS", "msid-semantic: WMS myavstream")
                .Replace("msid:-", "msid:myavstream").Replace("msid: ", "msid:myavstream ")
                .Replace("mslabel:", "mslabel:myavstream ");
        }
    }
}
