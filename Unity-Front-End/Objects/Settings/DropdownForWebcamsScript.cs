﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.MixedReality.WebRTC;
using Objects.Buttons;
using Services;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace Objects
{
    public class DropdownForWebcamsScript : MonoBehaviour
    {
        public static TMP_Dropdown Dropdown { get; private set; }
        [SerializeField] public TMP_Dropdown dropdownForWebcamResolutions;


        private void Awake()
        {
            Dropdown = gameObject.GetComponent<TMP_Dropdown>();
            Dropdown.AddOptions(DevicesService.Instance.GetAllWebcamNames());
            Dropdown.onValueChanged.AddListener(OnValueChanged);
        }

        private async Task ChangeDropdownOptions(int optionValue)
        {
            dropdownForWebcamResolutions.interactable = true;
            var resolutions =
                await DevicesService.Instance.GetResolutionsForWebcamByName(Dropdown
                    .options[optionValue].text);
            dropdownForWebcamResolutions.AddOptions(resolutions);
        }

        private async void OnValueChanged(int evenData)
        {
            dropdownForWebcamResolutions.options.Clear();
            if (evenData.Equals(0))
            {
                dropdownForWebcamResolutions.interactable = false;
                dropdownForWebcamResolutions.gameObject.transform.Find("Label").GetComponent<TMP_Text>().text =
                    String.Empty;
            }
            else
            {
                await ChangeDropdownOptions(evenData);
                StartButtonScript.Button.interactable = true;
            }
        }


        private void Start()
        {
        }

        private void Update()
        {
            if (!Dropdown.IsInteractable())
            {
                dropdownForWebcamResolutions.interactable = false;
            }
        }
    }
}
