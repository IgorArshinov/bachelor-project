﻿namespace Models.Interfaces
{
    public interface ISetting
    {
        int Width { get; set; }
        int Height { get; set; }
        int FramesPerSecond { get; set; }
    }
}
