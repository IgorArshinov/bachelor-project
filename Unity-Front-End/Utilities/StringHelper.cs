﻿using System.Text;
using System.Text.RegularExpressions;

namespace Utilities
{
    public static class StringHelper
    {
        public static string UnderscoreBeforeCaps(string stringToChange)
        {
            return Regex.Replace(stringToChange, "(.)([A-Z])", "$1_$2");
        }
    }
}
