﻿namespace Enums
{
    public static class IntentEnum
    {
        public const string SelectAffectiveSliderValuesAfterTask = "Select_Affective_Slider_Values_After_Task";
        public const string SmallTalkAboutLanguage = "Small_Talk_About_Language";
        public const string DescribeYourEmotionInOneWord = "Describe_Your_Emotion_In_One_Word";

        public const string SmallTalkAboutEmotionsAndQuestionAboutRobotEmotions =
            "Small_Talk_About_Emotions_And_Question_About_Robot_Emotions";

        public const string SmallTalkAboutFavoriteArtists = "Small_Talk_About_Favorite_Artists";
        public const string Introduction = "Introduction";
        public const string SelectAffectiveSliderValues = "Select_Affective_Slider_Values";
        public const string FormForMetadata = "Form_For_Metadata";
        public const string FormForAffectiveValuesForEmotions = "Form_For_Affective_Values_For_Emotions";
        public const string ExplanationAboutTasks = "Explanation_About_Tasks";
        public const string TaskQuestionSadness = "Task_Question_Sadness";

        public const string DescribeYourEmotionInMultipleSentencesAfterTask =
            "Describe_Your_Emotion_In_Multiple_Sentences_After_Task";

        public const string DescribeYourEmotionInOneWordAfterTask = "Describe_Your_Emotion_In_One_Word_After_Task";
        public const string EndSession = "End_Session";
        public const string TaskQuestionFatigue = "Task_Question_Fatigue";
        public const string TaskQuestionJoviality = "Task_Question_Joviality";
        public const string TaskQuestionAttentiveness = "Task_Question_Attentiveness";
    }
}
