﻿using System;
using System.Linq;

namespace Utilities
{
    public static class ArrayHelper
    {
        public static T[] ConcatArrays<T>(params T[][] p)
        {
            var position = 0;
            var outputArray = new T[p.Sum(a => a.Length)];
            foreach (var curr in p)
            {
                Array.Copy(curr, 0, outputArray, position, curr.Length);
                position += curr.Length;
            }

            return outputArray;
        }
    }
}
