﻿using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Video;
using UnityToPython;
using Utilities;

namespace Objects
{
    public class VideoPlayerScript : MonoBehaviour
    {
        public static VideoPlayer VideoPlayer { get; set; }

        private void Awake()
        {
            VideoPlayer = gameObject.GetComponent<VideoPlayer>();
            VideoPlayer.loopPointReached += VideoIsFinished;
        }

        private void VideoIsFinished(VideoPlayer source)
        {
            if (RtcServer.Instance.Signaler.PeerConnection.IsConnected)
            {
                UnityToPythonDataChannel.Instance.SendMessage(
                    JsonHelper.CreateJsonObjectWithoutData("video_is_stopped"));
            }
        }


        private void Start()
        {
        }

        private void Update()
        {
        }

        public static void StopVideoClip()
        {
            VideoPlayer.Stop();
        }

        public static void LoadVideoClip(string videoClip)
        {
            VideoClip clip = Resources.Load<VideoClip>("Videos/" + videoClip) as VideoClip;
            VideoPlayer.clip = clip;
            VideoPlayer.targetTexture.Release();
        }
    }
}
