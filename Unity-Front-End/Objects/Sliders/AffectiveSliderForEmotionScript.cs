﻿using UnityEngine;
using UnityEngine.UI;

namespace Objects.Sliders
{
    public class AffectiveSliderForEmotionScript : MonoBehaviour
    {
        [SerializeField] private string emotionLabel;
        [SerializeField] private Slider arousalSlider;
        [SerializeField] private Slider valenceSlider;

        private void Start()
        {
        }

        private void Update()
        {
        }

        public object GetValues()
        {
            return new
            {
                emotion_label = emotionLabel, arousal_value = arousalSlider.value,
                valence_value = valenceSlider.value
            };
        }
    }
}
