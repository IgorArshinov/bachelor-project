﻿using Objects;
using Strategies.Intents.Interfaces;
using Strategies.JsonObjects.Interfaces;
using UnityEngine;

namespace Strategies.JsonObjects
{
    public class ConnectedState : IStrategy
    {
        public string Type { get; set; }
        public string Data { get; set; }
        public string ID { get; set; }

        public void MakeChangesInUnity()
        {
            MainThreadDispatcherScript.Instance()
                .Enqueue(() =>
                {
                    PopUpContainerScript.PopUpContainer.SetActive(false);
                    GUIContainerScript.GUIContainer.transform.Find("StartPanel").gameObject
                        .SetActive(false);
                    GameObjectManagerScript.GameObjects["loadingWhenConnecting"].SetActive(false);
                });
        }
    }
}
