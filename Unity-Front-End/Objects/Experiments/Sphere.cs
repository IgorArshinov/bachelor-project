﻿using Newtonsoft.Json;
using UnityEngine;
using UnityToPython;

namespace Objects
{
    public class Sphere : MonoBehaviour
    {
        private RtcServer _rtcServer;

        void Start()
        {
            _rtcServer = FindObjectOfType(typeof(RtcServer)) as RtcServer;
             
        }

        void Update()
        {
        }

        void OnMouseDown()
        {
            Debug.Log("OnMouseDown: " + this.name);
            if (_rtcServer.Signaler.PeerConnection.IsConnected)
            {
                var jsonObject = JsonConvert.SerializeObject(new {type = "play"});

                UnityToPythonDataChannel.Instance.SendMessage(jsonObject);
            }
        }
    }
}
