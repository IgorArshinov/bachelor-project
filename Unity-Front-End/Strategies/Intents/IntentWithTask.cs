﻿using Objects;
using Strategies.Intents.Interfaces;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

namespace Strategies.Intents
{
    public class IntentWithTask : IIntentStrategy
    {
        private string IntentName { get; set; }
        private string VideoName { get; }

        public IntentWithTask(string intentName, string videoName)
        {
            IntentName = intentName;
            VideoName = videoName;
        }

        public void MakeChangesInUnity(string fulfillmentText)
        {
            GUIHelper.SetAllPanelsToInactive();
            GUIHelper.AddTextToChatbotTextPanel(fulfillmentText);
            GUIHelper.SetPanelToActive(IntentName);
            MainThreadDispatcherScript.Instance().Enqueue(() =>
            {
                GameObjectManagerScript.GameObjects["affectiveSlider"].GetComponent<Slider>().value = 50;
                GameObjectManagerScript.GameObjects["valenceSlider"].GetComponent<Slider>().value = 50;
                VideoPlayerScript.LoadVideoClip(VideoName);
            });
        }
    }
}
