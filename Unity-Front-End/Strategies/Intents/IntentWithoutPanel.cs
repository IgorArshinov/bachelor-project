﻿using Objects;
using Strategies.Intents.Interfaces;
using UnityEngine;
using Utilities;

namespace Strategies.Intents
{
    public class IntentWithoutPanel : IIntentStrategy
    {
        public void MakeChangesInUnity(string fulfillmentText)
        {
            GUIHelper.SetAllPanelsToInactive();
            GUIHelper.AddTextToChatbotTextPanel(fulfillmentText);
        }
    }
}
