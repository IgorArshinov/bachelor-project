import os
import sys

from constants.paths import ROOT_DIR

WEBRTC_ID = "Python_client"

if getattr(sys, 'frozen', False):
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = os.path.join(sys._MEIPASS,
                                                                ROOT_DIR + "/constants/Chatbot-4df8991ab9a5.json")
else:
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = ROOT_DIR + '/constants/Chatbot-4df8991ab9a5.json'

DIALOGFLOW_PROJECT_ID = 'chatbot-294711'
DIALOGFLOW_LANGUAGE_CODE = 'en'
SESSION_ID = 'me'
