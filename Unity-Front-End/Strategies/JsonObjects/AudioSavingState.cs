﻿using Objects;
using Strategies.Intents.Interfaces;
using Strategies.JsonObjects.Interfaces;

namespace Strategies.JsonObjects
{
    public class AudioSavingState : IStrategy
    {
        public string Type { get; set; }
        public string Data { get; set; }
        public string ID { get; set; }

        public void MakeChangesInUnity()
        {
            MainThreadDispatcherScript.Instance().Enqueue(() =>
            {
                GameObjectManagerScript.GameObjects["loadingWhenAudioIsSaving"].SetActive(true);
            });
        }
    }
}
