﻿using UnityEngine;

namespace Objects
{
    public class GUIContainerScript : MonoBehaviour
    {
        public static GameObject GUIContainer { get; private set; }

        private void Awake()
        {
            GUIContainer = gameObject;
        }

        private void Start()
        {
        }

        private void Update()
        {
        }
    }
}
