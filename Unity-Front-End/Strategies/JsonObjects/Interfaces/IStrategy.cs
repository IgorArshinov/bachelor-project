﻿namespace Strategies.JsonObjects.Interfaces
{
    public interface IStrategy
    {
        string Type { get; set; }
        string Data { get; set; }
        string ID { get; set; }
        void MakeChangesInUnity();
    }
}
