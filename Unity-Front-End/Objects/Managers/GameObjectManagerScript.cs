﻿using System.Collections.Generic;
using UnityEngine;

namespace Objects
{
    public class GameObjectManagerScript : MonoBehaviour
    {
        [SerializeField] private GameObject loadingWhenAudioIsSaving;
        [SerializeField] private GameObject loadingWhenConnecting;
        [SerializeField] private GameObject popUpContainer;
        [SerializeField] private GameObject menuPanel;
        [SerializeField] private GameObject thankYouPanel;
        [SerializeField] private GameObject affectiveSlider;
        [SerializeField] private GameObject valenceSlider;
        [SerializeField] private GameObject toggleEmotionsAfterTask;
        public static IDictionary<string, GameObject> GameObjects;

        private void Awake()
        {
            GameObjects = new Dictionary<string, GameObject>
            {
                {"loadingWhenAudioIsSaving", loadingWhenAudioIsSaving},
                {"popUpContainer", popUpContainer},
                {"menuPanel", menuPanel},
                {"thankYouPanel", thankYouPanel},
                {"affectiveSlider", affectiveSlider},
                {"valenceSlider", valenceSlider},
                {"toggleEmotionsAfterTask", toggleEmotionsAfterTask},
                {
                    "loadingWhenConnecting", loadingWhenConnecting
                }
            };
        }

        private void Start()
        {
        }

        private void Update()
        {
        }
    }
}
