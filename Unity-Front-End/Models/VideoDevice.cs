﻿using Microsoft.MixedReality.WebRTC;
using Models.Interfaces;

namespace Models
{
    public class VideoDevice : ISetting
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int FramesPerSecond { get; set; }
        public VideoCaptureDevice VideoCaptureDevice { get; set; }
    }
}
