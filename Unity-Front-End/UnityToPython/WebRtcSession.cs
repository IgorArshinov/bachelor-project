﻿using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using WebSocketSharp;
using WebSocketSharp.Server;

namespace UnityToPython
{
    public class WebRtcSession : WebSocketBehavior
    {

        public event System.Action<string> messageReceived;
        public event System.Action socketOpen;
        public event System.Action socketClosed;
        public event System.Action<string> connectionError;

        private Task _timeout;
        private bool _connected = false;

        private void TimeoutConnection()
        {
            Thread.Sleep(500);
            if (!_connected)
            {
                connectionError?.Invoke("Client did not open socket in 500 ms.");
            }
        }

        public WebRtcSession() : base()
        {
            // if the socket connection isn't opened after 500ms something is wrong and we will close the socket.
            _timeout = Task.Factory.StartNew(TimeoutConnection, TaskCreationOptions.LongRunning);
        }

        protected override void OnMessage(MessageEventArgs e)
        {
            messageReceived?.Invoke(e.Data);
        }

        protected override void OnOpen()
        {
            _connected = true;
            base.OnOpen();
            socketOpen?.Invoke();
        }

        protected override void OnClose(CloseEventArgs e)
        {
            Debug.Log("OnCloneEvent");
            base.OnClose(e);
            socketClosed?.Invoke();
        }

    }
}
