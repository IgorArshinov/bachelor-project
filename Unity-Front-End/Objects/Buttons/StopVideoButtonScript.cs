﻿using UnityEngine;
using UnityEngine.UI;
using UnityToPython;
using Utilities;

namespace Objects.Buttons
{
    public class StopVideoButtonScript : MonoBehaviour
    {
        private Button Button { get; set; }

        private void Awake()
        {
            Button = gameObject.GetComponent<Button>();
            Button.onClick.AddListener(OnClick);
        }

        private void Start()
        {
        }

        private void Update()
        {
        }

        private void OnClick()
        {
            if (RtcServer.Instance.Signaler.PeerConnection.IsConnected)
            {
                UnityToPythonDataChannel.Instance.SendMessage(
                    JsonHelper.CreateJsonObjectWithoutData("video_is_stopped"));

                var finishedButtonForTask = FinishedButtonForTaskScript.Button;
                if (!finishedButtonForTask.IsInteractable())
                {
                    finishedButtonForTask.interactable = true;
                }
            }
        }
    }
}
