import json
from abc import ABC, abstractmethod

from constants.session_settings import WEBRTC_ID
from enums.intent_enum import Intents
from singletons.current_session import CurrentSession
from singletons.current_subject import CurrentSubject
from utilities import channel_send


class StateContext:

    def __init__(self):
        self.__state = None

    async def transition_to(self, state):
        self.__state = state
        self.__state.context = self
        await self.__state.execute_actions()


class State(ABC):

    def __init__(self):
        self.__context = None

    @property
    def context(self) -> StateContext:
        return self.__context

    @context.setter
    def context(self, context: StateContext) -> None:
        pass

    @abstractmethod
    def execute_actions(self):
        pass


class DisconnectedState(State):
    async def execute_actions(self):
        print("Client is disconnected")


class ConnectedState(State):
    async def execute_actions(self):
        await channel_send(CurrentSession.get_instance().channel,
                           json.dumps({"id": WEBRTC_ID, "type": "connected_state"}))

        await CurrentSession.get_instance().set_current_intent_and_start_dialogflow_event(
            Intents.INTRODUCTION)


class AudioSavingState(State):
    async def execute_actions(self):
        await channel_send(CurrentSession.get_instance().channel,
                           json.dumps({"id": WEBRTC_ID, "type": "audio_saving_state"}))


class ChatbotWaitsForAudioInputState(State):
    async def execute_actions(self):
        CurrentSubject.get_instance().chatbot_waits_for_audio_input = True


class VideoIsPlayingState(State):
    async def execute_actions(self):
        CurrentSubject.get_instance().is_watching_video = True


class VideoIsStoppedState(State):
    async def execute_actions(self):
        CurrentSubject.get_instance().is_watching_video = False


class DialogState(State):
    async def execute_actions(self) -> None:
        await channel_send(CurrentSession.get_instance().channel,
                           json.dumps({"id": WEBRTC_ID, "type": "dialog_state"}))
