﻿using Newtonsoft.Json;
using Objects;
using Strategies.Intents;
using Strategies.Intents.Interfaces;
using Strategies.JsonObjects.Interfaces;
using UnityEngine;

namespace Strategies.JsonObjects
{
    public class Intent : IStrategy
    {
        public string Type { get; set; }
        public string Data { get; set; }
        public string ID { get; set; }
        [JsonProperty("current_intent_name")] public string currentIntentName { get; set; }
        [JsonProperty("chatbot_waits_for_audio_input")] public bool chatbotWaitsForAudioInput { get; set; }
        [JsonProperty("fulfillment_text")] public string fulfillmentText { get; set; }

        public void MakeChangesInUnity()
        {
            IntentContext.Instance.SetStrategy(currentIntentName);
            IntentContext.Instance.Name = currentIntentName;
            IntentContext.Instance.ChatbotWaitsForAudioInput = chatbotWaitsForAudioInput;
            MainThreadDispatcherScript.Instance().Enqueue(() =>
            {
                IntentContext.Instance.ExecuteStrategy(fulfillmentText);
            });
        }
    }
}
