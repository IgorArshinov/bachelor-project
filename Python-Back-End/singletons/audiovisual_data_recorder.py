import asyncio
import audioop
import time
from timeit import default_timer as timer

import av
import continuous_threading
import webrtcvad
from aiortc.mediastreams import MediaStreamError
from av.container.output import OutputContainer
from av.stream import Stream
from pyannote.audio.utils.signal import Binarize
from pyannote.core import Annotation
from pydub import AudioSegment
from torch import hub

from constants.audio_settings import FREQUENCY_SPEED, SAMPLE_WIDTH
from constants.paths import WAV_FILE_TYPE, ROOT_DIR
from singletons.current_session import CurrentSession
from singletons.current_subject import CurrentSubject
from singletons.logger import Logger
from states import AudioSavingState, DialogState
from strategies.intents.intent_context import IntentContext


class AudiovisualDataRecorder:
    __instance = None

    @staticmethod
    def get_instance():
        if AudiovisualDataRecorder.__instance is None:
            AudiovisualDataRecorder()
        return AudiovisualDataRecorder.__instance

    def __init__(self):
        self.__audio_frames = []
        self.__video_frames = []
        self.__keep_detecting_speech_every_x_seconds_task = continuous_threading.PausableThread(
            target=self.keep_detecting_speech_every_x_seconds, args=())
        self.__vad = webrtcvad.Vad(3)
        self.started_voice_detection = False
        self.stopped_voice_detection = True
        self.reset_vad = False
        self.init = False
        self.is_speech = False
        self.record_for_dialogflow = False
        self.start_sentence = False
        self.start_dialogflow_timer = 0
        self.current_session = CurrentSession.get_instance()
        self.__tracks = []
        self.__tasks = []
        self.bytes_frame = None
        self.pipeline = hub.load(ROOT_DIR + '/machine_learning_models/pyannote_pyannote-audio_master', 'sad_ami',
                                 pipeline=False, batch_size=2,
                                 source="local")
        self.binarize = Binarize(offset=0.9, onset=0.9, log_scale=True, min_duration_off=0.1, min_duration_on=0.1)
        if AudiovisualDataRecorder.__instance is not None:
            raise Exception("This class is a singleton!")
        else:
            AudiovisualDataRecorder.__instance = self

    def add_track(self, track):
        self.__tracks.append(track)

    async def start(self):

        for track in self.__tracks:
            self.__tasks.append(asyncio.ensure_future(self.__run_track(track)))

    async def stop(self):
        self.__keep_detecting_speech_every_x_seconds_task.stop()
        for task in self.__tasks:
            task.cancel()

    async def __run_track(self, track):
        frame_to_log = await track.recv()
        Logger.get_instance().logger.info(frame_to_log)
        while True:
            try:
                frame = await track.recv()
                if type(frame) is av.video.frame.VideoFrame:
                    pass

                if type(frame) is av.audio.frame.AudioFrame:
                    self.bytes_frame = frame.to_ndarray().tobytes()
                    self.__audio_frames.append(self.bytes_frame)

                    if CurrentSubject.get_instance().chatbot_waits_for_audio_input and self.started_voice_detection is False and CurrentSubject.get_instance().is_watching_video is False:
                        self.keep_detecting_speech_every_x_seconds_task.start()

                        self.stopped_voice_detection = False
                        self.started_voice_detection = True
                        Logger.get_instance().logger.info("keep_detecting_loud_sounds_every_x_seconds_task.start()")
                    elif (
                            CurrentSubject.get_instance().chatbot_waits_for_audio_input is False and self.stopped_voice_detection is False) or (
                            CurrentSubject.get_instance().is_watching_video is True and self.stopped_voice_detection is False):

                        if self.record_for_dialogflow is True:
                            self.save_audio_for_dialogflow()
                            await self.current_session.get_instance().state_context.transition_to(DialogState())

                        if self.is_speech is True:
                            self.set_subject_is_not_talking()

                        self.keep_detecting_speech_every_x_seconds_task.stop()

                        self.stopped_voice_detection = True
                        self.started_voice_detection = False

                        Logger.get_instance().logger.info("keep_detecting_loud_sounds_every_x_seconds_task.stop()")

                        if CurrentSubject.get_instance().current_intent_name.startswith("Task_Question_") is False:
                            await self.detect_intent_and_react()

                    if (self.started_voice_detection is False or self.record_for_dialogflow is False) and len(
                            self.__audio_frames) >= 600:
                        self.__audio_frames = self.__audio_frames[-100:]
                        Logger.get_instance().logger.info("Audio %s", len(self.__audio_frames))

                    if self.reset_vad is True and self.started_voice_detection is True:
                        new_frame = audioop.tomono(self.bytes_frame, 4, 1, 1)
                        self.__vad.is_speech(new_frame, 48000)


            except MediaStreamError:
                await self.stop()
                Logger.get_instance().logger.info(MediaStreamError)
                return

    @property
    def keep_detecting_speech_every_x_seconds_task(self):
        return self.__keep_detecting_speech_every_x_seconds_task

    def keep_detecting_speech_every_x_seconds(self):
        if self.init is False:
            self.init = True
            self.start_sentence = False

        if self.is_speech is True:

            self.reset_vad = True

            asyncio.run_coroutine_threadsafe(
                self.current_session.get_instance().state_context.transition_to(AudioSavingState()),
                self.current_session.loop)

            start = timer()
            if self.record_for_dialogflow is False:
                self.start_dialogflow_timer = timer()
                time.sleep(2)
            else:
                time.sleep(3)
                self.start_sentence = True

            end_pyannote = timer()
            total_seconds = end_pyannote - start
            Logger.get_instance().logger.info("total_seconds %s" % total_seconds)

            audio_segment = AudioSegment(

                bytes().join(self.__audio_frames[-int(total_seconds * 50 + 10):]),
                frame_rate=FREQUENCY_SPEED,
                sample_width=SAMPLE_WIDTH,
                channels=2
            )

            audio_segment: AudioSegment = audio_segment.set_frame_rate(16000)
            audio_segment = audio_segment.set_channels(1)

            audio_segment.export("speech_detection/pyannote.wav", format="wav")
            filtered_audio: Annotation = self.pipeline({'audio': "speech_detection/pyannote.wav"})
            speech = self.binarize.apply(filtered_audio, dimension=1)
            Logger.get_instance().logger.info("speech %s" % speech)
            if len(speech) == 0:

                if self.record_for_dialogflow is True:
                    self.save_audio_for_dialogflow()

                asyncio.run_coroutine_threadsafe(
                    self.current_session.get_instance().state_context.transition_to(DialogState()),
                    self.current_session.loop)
                self.set_subject_is_not_talking()
                audio_segment = None
            else:
                self.record_for_dialogflow = True
                self.is_speech = True
                if self.start_sentence is False:
                    time.sleep(3)
                else:
                    time.sleep(2)
                end_recognize_emotion = timer()






        else:
            time.sleep(0.05)
            new_frame = audioop.tomono(self.bytes_frame, 4, 1, 1)
            if new_frame is not None:
                self.is_speech = self.__vad.is_speech(new_frame, 48000)

    def add_audio_during_task_and_set_label(self):
        audios_from_task = next(task for task in CurrentSubject.get_instance().audios_from_tasks if
                                task["id"] == CurrentSubject.get_instance().current_task_emotion)
        audios_from_task["audios"].append({"id": CurrentSubject.get_instance().current_intent_name,
                                           "path": CurrentSubject.get_instance().current_saving_path_for_audio,
                                           "emotion_label": CurrentSubject.get_instance().current_task_emotion})

    async def detect_intent_and_react(self):
        IntentContext.get_instance().set_strategy(
            CurrentSubject.get_instance().current_intent_name)
        await IntentContext.get_instance().execute_strategy()

    def save_user_audio_input_for_task(self, seconds):
        audio_segment = AudioSegment(
            bytes().join(self.__audio_frames[-int(seconds * 50 + 10):]),
            frame_rate=FREQUENCY_SPEED,
            sample_width=SAMPLE_WIDTH,
            channels=2
        )
        audio_segment = audio_segment.set_frame_rate(16000)
        audio_segment = audio_segment.set_channels(1)

        audio_segment.export(CurrentSubject.get_instance().current_saving_path_for_audio, format="wav")
        CurrentSubject.get_instance().current_task_file_number += 1

    def save_user_audio_input_for_dialogflow_and_get_response(self, seconds):
        audio_segment = AudioSegment(
            bytes().join(self.__audio_frames[-int(seconds * 50 + 10):]),
            frame_rate=FREQUENCY_SPEED,
            sample_width=SAMPLE_WIDTH,
            channels=2
        )
        audio_segment = audio_segment.set_frame_rate(16000)
        audio_segment = audio_segment.set_channels(1)

        audio_segment.export(CurrentSubject.get_instance().current_saving_path_for_audio, format="wav")

        CurrentSubject.get_instance().chatbot_waits_for_audio_input = False

    def save_user_video_input(self, total_seconds):
        frames = self.__video_frames[-int(total_seconds * 30 + 6):].copy()

        output: OutputContainer = av.open(CurrentSubject.get_instance().current_saving_path_for_video, 'w')

        stream: Stream = output.add_stream('mpeg4', rate=30)
        output.streams.video[0].thread_type = 'AUTO'

        stream.codec_context.skip_frame = 'ALL'
        for frame in frames:

            frame.pts = None
            frame.time_base = None
            print(frame)
            for packet in stream.encode(frame):
                print(packet)
                output.mux(packet)

        output.close()
        frames = None
        stream = None
        output = None
        self.__video_frames.clear()

    def save_audio_for_dialogflow(self):
        end_dialogflow_timer = timer()

        total_seconds = end_dialogflow_timer - self.start_dialogflow_timer
        if CurrentSubject.get_instance().current_intent_name.startswith("Task_Question_"):

            CurrentSubject.get_instance().current_saving_path_for_audio = CurrentSubject.get_instance().get_path_to_subject_dialogflow_audio_task_directory() + CurrentSubject.get_instance().current_intent_name + "_" + str(
                CurrentSubject.get_instance().current_task_file_number) + WAV_FILE_TYPE
            self.save_user_audio_input_for_task(total_seconds)

            self.add_audio_during_task_and_set_label()

        else:
            if CurrentSubject.get_instance().is_doing_tasks:
                CurrentSubject.get_instance().current_saving_path_for_audio = CurrentSubject.get_instance().get_path_to_subject_dialogflow_audio_task_directory() + CurrentSubject.get_instance().current_intent_name + WAV_FILE_TYPE
            else:
                CurrentSubject.get_instance().current_saving_path_for_audio = CurrentSubject.get_instance().get_path_to_subject_dialogflow_audio_directory() + CurrentSubject.get_instance().current_intent_name + WAV_FILE_TYPE

            self.save_user_audio_input_for_dialogflow_and_get_response(total_seconds)

        self.__audio_frames = self.__audio_frames[-100:]

        self.record_for_dialogflow = False
        self.start_sentence = False

    def set_subject_is_not_talking(self):
        self.is_speech = False
        self.reset_vad = False
