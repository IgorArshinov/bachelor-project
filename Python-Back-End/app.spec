# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['cli.py'],
             pathex=['D:\\art\\PyCharm\\projects\\Bachelor-Project-Clean\\webrtc_client'],
             binaries=[],
             datas=[],
             hiddenimports=['pyannote.audio.features','pyannote.audio.Features', 'pydub','pyannote.core','pyannote.audio.utils.signal','yaml','dialogflow','pkg_resources.Distribution','scipy.spatial.transform._rotation_groups', 'pkg_resources.py2_warn', 'sklearn.utils._weight_vector', 'sklearn.utils.lgamma', 'pysqlite2', "MySQLdb", "psycopg2", "sqlalchemy.sql.functions.func", "pkg_resources.markers"],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
a.datas += [('webrtc_client\\constants\\Chatbot-4df8991ab9a5.json', 'D:\\art\\PyCharm\\projects\\Bachelor-Project-Clean\\webrtc_client\\constants\\Chatbot-4df8991ab9a5.json', 'DATA')]
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='cli',
          debug=True,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='cli')
