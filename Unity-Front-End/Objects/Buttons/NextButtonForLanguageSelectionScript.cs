﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityToPython;
using Utilities;

namespace Objects
{
    public class NextButtonForLanguageSelectionScript : MonoBehaviour
    {
        private Button Button { get; set; }
        [SerializeField] private TMP_Dropdown dropdown;

        private void Awake()
        {
            Button = gameObject.GetComponent<Button>();
            Button.onClick.AddListener(OnClick);
        }

        private void Start()
        {
        }

        private void Update()
        {
        }

        private void OnClick()
        {
            var selectedLanguage = dropdown.options[dropdown.value].text;
            var data = new
            {
                form_name = "language_selection",
                form_data = selectedLanguage,
            };
            if (RtcServer.Instance.Signaler.PeerConnection.IsConnected)
            {
                UnityToPythonDataChannel.Instance.SendMessage(
                    JsonHelper.CreateJsonObjectWithData("input_form", data));
            }
        }
    }
}
