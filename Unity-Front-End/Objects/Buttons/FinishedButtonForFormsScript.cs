﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityToPython;
using Utilities;

namespace Objects.Buttons
{
    public class FinishedButtonForFormsScript : MonoBehaviour
    {
        public static Button Button { get; private set; }

        [SerializeField] private GameObject[] toggleGroupForms;
        [SerializeField] private GameObject[] inputFieldForms;

        private ToggleGroup[] _toggleGroups = new ToggleGroup[0];

        private void Awake()
        {
            Button = gameObject.GetComponent<Button>();
            Button.onClick.AddListener(OnClick);

            foreach (var form in toggleGroupForms)
            {
                _toggleGroups = _toggleGroups.Concat(form.GetComponentsInChildren<ToggleGroup>()).ToArray();
                foreach (var toggleGroup in _toggleGroups)
                {
                    var toggles = toggleGroup.GetComponentsInChildren<Toggle>();


                    foreach (var toggle in toggles)
                    {
                        toggle.onValueChanged.AddListener(OnValueChangedInToggles);
                    }
                }
            }
        }

        private void OnValueChangedInToggles(bool isOn)
        {
            if (CheckAllToggleGroups())
            {
                Button.interactable = true;
            }
            else
            {
                Button.interactable = false;
            }
        }

        private bool CheckAllToggleGroups()
        {
            bool areActive = true;
            foreach (var toggleGroup in _toggleGroups)
            {
                if (!toggleGroup.ActiveToggles().FirstOrDefault())
                {
                    areActive = false;
                }
            }

            return areActive;
        }

        private IDictionary<string, object> GetFormsData()
        {
            IDictionary<string, object> values = new Dictionary<string, object>();
             
            for (int f = 0; f < inputFieldForms.Length; f++)
            {
                var inputFields = inputFieldForms[f].GetComponentsInChildren<TMP_InputField>();
                var arrayForAnswers = new object[inputFields.Length];
                for (int i = 0, j = 1; i < inputFields.Length; i++, j++)
                {
                    arrayForAnswers[i] = new
                    {
                        question = j,
                        answer_data = inputFields[i].text
                    };
                }

                values.Add(StringHelper.UnderscoreBeforeCaps(inputFieldForms[f].name).ToLower(), new
                {
                    form_name = StringHelper.UnderscoreBeforeCaps(inputFieldForms[f].name).ToLower(),
                    form_data = arrayForAnswers
                });
            }

            for (int f = 0; f < toggleGroupForms.Length; f++)
            {
                var toggleGroups = toggleGroupForms[f].GetComponentsInChildren<ToggleGroup>();

                var arrayForAnswers = new object[toggleGroups.Length];

                for (int i = 0, j = 1; i < toggleGroups.Length; i++, j++)
                {
                    if (toggleGroups[i].ActiveToggles().First())
                    {
                        int number;
                        bool isParsable = Int32.TryParse(toggleGroups[i].ActiveToggles().First().name, out number);
                        object data;
                        if (isParsable)
                        {
                            data = number;
                        }
                        else
                        {
                            data = toggleGroups[i].ActiveToggles().First().name;
                        }

                        arrayForAnswers[i] = new
                        {
                            answer_number = j,
                            answer_data = data
                        };
                    }
                }

                 
                 
                 
                 
                 
                values.Add(StringHelper.UnderscoreBeforeCaps(toggleGroupForms[f].name).ToLower(), new
                {
                    form_name = StringHelper.UnderscoreBeforeCaps(toggleGroupForms[f].name).ToLower(),
                    form_data = arrayForAnswers
                });
            }

            return values;
        }

        private void OnClick()
        {
            var formData = GetFormsData();

            GameObjectManagerScript.GameObjects["popUpContainer"].SetActive(true);
            GameObjectManagerScript.GameObjects["thankYouPanel"].SetActive(true);

            if (RtcServer.Instance.Signaler.PeerConnection.IsConnected)
            {
                UnityToPythonDataChannel.Instance.SendMessage(
                    JsonHelper.CreateJsonObjectWithData("input_form", formData));
            }
        }

        private void Start()
        {
        }

        private void Update()
        {
        }
    }
}
