﻿using Strategies.Intents.Interfaces;
using Strategies.JsonObjects.Interfaces;

namespace Strategies.JsonObjects
{
    public class StrategyContext
    {
        private IStrategy _strategy;

        public StrategyContext()
        {
        }

        public StrategyContext(IStrategy strategy)
        {
            _strategy = strategy;
        }


        public void SetStrategy(IStrategy strategy)
        {
            _strategy = strategy;
        }

        public void ExecuteStrategy()
        {
            _strategy.MakeChangesInUnity();
        }
    }
}
