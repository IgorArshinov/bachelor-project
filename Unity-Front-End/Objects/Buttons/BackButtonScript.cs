﻿using UnityEngine;
using UnityEngine.UI;

namespace Objects.Buttons
{
    public class BackButtonScript : MonoBehaviour
    {
        private Button Button { get; set; }

        private void Awake()
        {
            Button = gameObject.GetComponent<Button>();
            Button.onClick.AddListener(OnClick);
        }

        private void OnClick()
        {
            GameObjectManagerScript.GameObjects["menuPanel"].SetActive(false);
            PopUpContainerScript.PopUpContainer.SetActive(false);
        }
    }
}
