﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityToPython;
using Utilities;

namespace Objects.Buttons
{
    public class FinishedButtonForFormScript : MonoBehaviour
    {
        [SerializeField] private GameObject form;
        private Button _button;
        private TMP_InputField[] _inputFields;

        private void Awake()
        {
            _button = gameObject.GetComponent<Button>();
            _button.onClick.AddListener(OnClick);
            _inputFields = form.GetComponentsInChildren<TMP_InputField>();

            for (int i = 0; i < _inputFields.Length; i++)
            {
                _inputFields[i].onValueChanged.AddListener(OnValueChangeCheck);
            }
        }

        private void OnValueChangeCheck(string data)
        {
            if (CheckAllInputFields())
            {
                _button.interactable = true;
            }
            else
            {
                _button.interactable = false;
            }
        }

        private bool CheckAllInputFields()
        {
            bool areActive = true;
            foreach (var inputField in _inputFields)
            {
                if (String.IsNullOrEmpty(inputField.text))
                {
                    areActive = false;
                }
            }

            return areActive;
        }

        private void Start()
        {
        }

        private void Update()
        {
        }

        private void OnClick()
        {
            var dropdowns = form.GetComponentsInChildren<TMP_Dropdown>();

            IDictionary<string, object> values = new Dictionary<string, object>();


            for (int i = 0; i < _inputFields.Length; i++)
            {
                values.Add(StringHelper.UnderscoreBeforeCaps(_inputFields[i].name).ToLower(), _inputFields[i].text);
            }


            for (int i = 0; i < dropdowns.Length; i++)
            {
                var dropdown = dropdowns[i];
                values.Add(
                    StringHelper.UnderscoreBeforeCaps(StringHelper.UnderscoreBeforeCaps(dropdown.name).ToLower())
                        .ToLower(), dropdown.options[dropdown.value].text);
            }


            var data = new
            {
                form_name = form.name.ToLower(),
                form_data = values
            };


            if (RtcServer.Instance.Signaler.PeerConnection.IsConnected)
            {
                UnityToPythonDataChannel.Instance.SendMessage(
                    JsonHelper.CreateJsonObjectWithData("input_form", data));
            }
        }
    }
}
