﻿using UnityEngine;

namespace Objects
{
    public class PopUpContainerScript : MonoBehaviour
    {
        public static GameObject PopUpContainer { get; private set; }

        private void Awake()
        {
            PopUpContainer = gameObject;
        }

        private void Start()
        {
        }

        private void Update()
        {
        }
    }
}
