﻿using UnityEngine;
using UnityEngine.UI;
using UnityToPython;
using Utilities;

namespace Objects.Buttons
{
    public class FinishedButtonForTaskScript : MonoBehaviour
    {
        public static Button Button { get; set; }
        private GameObject LoadingWhenAudioIsSaving { get; set; }

        private void Awake()
        {
            Button = gameObject.GetComponent<Button>();
            Button.onClick.AddListener(OnClick);
        }


        private void Start()
        {
            LoadingWhenAudioIsSaving = GameObjectManagerScript.GameObjects["loadingWhenAudioIsSaving"];
        }

        private void Update()
        {
            if (!VideoPlayerScript.VideoPlayer.isPlaying)
            {
                if (LoadingWhenAudioIsSaving.activeSelf)
                {
                    Button.interactable = false;
                }
                else
                {
                    Button.interactable = true;
                }
            }
        }

        private void OnClick()
        {
            VideoPlayerScript.StopVideoClip();
            if (RtcServer.Instance.Signaler.PeerConnection.IsConnected)
            {
                UnityToPythonDataChannel.Instance.SendMessage(
                    JsonHelper.CreateJsonObjectWithoutData("task"));
            }
        }
    }
}
