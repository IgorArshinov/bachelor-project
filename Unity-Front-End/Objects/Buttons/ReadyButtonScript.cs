﻿using UnityEngine;
using UnityEngine.UI;
using UnityToPython;
using Utilities;

namespace Objects.Buttons
{
    public class ReadyButtonScript : MonoBehaviour
    {
        private Button Button { get; set; }

        private void Awake()
        {
            Button = gameObject.GetComponent<Button>();
            Button.onClick.AddListener(OnClick);
        }

        private void Start()
        {
        }

        private void Update()
        {
        }

        private void OnClick()
        {
            if (RtcServer.Instance.Signaler.PeerConnection.IsConnected)
            {
                UnityToPythonDataChannel.Instance.SendMessage(
                    JsonHelper.CreateJsonObjectWithData("input_form", "ready"));
            }
        }
    }
}
