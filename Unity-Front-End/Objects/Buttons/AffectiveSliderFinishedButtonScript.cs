﻿using UnityEngine;
using UnityEngine.UI;
using UnityToPython;
using Utilities;

namespace Objects.Buttons
{
    public class AffectiveSliderFinishedButtonScript : MonoBehaviour
    {
        public static Button Button { get; private set; }
        [SerializeField] private Slider arousalSlider;
        [SerializeField] private Slider valenceSlider;

        private void Start()
        {
            Button = gameObject.GetComponent<Button>();
            Button.onClick.AddListener(OnClick);
        }

        private void OnClick()
        {
            if (RtcServer.Instance.Signaler.PeerConnection.IsConnected)
            {
                var data = new
                {
                    valence = arousalSlider.value / 100,
                    arousal = valenceSlider.value / 100
                };

                UnityToPythonDataChannel.Instance.SendMessage(JsonHelper.CreateJsonObjectWithData("input_form", data));
            }
        }
    }
}
