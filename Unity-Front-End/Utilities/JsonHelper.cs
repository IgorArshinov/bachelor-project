﻿using Newtonsoft.Json;
using Strategies;
using Strategies.Intents;
using UnityEngine;
using UnityToPython;

namespace Utilities
{
    public static class JsonHelper
    {
        public static string CreateJsonObjectWithData(string type, object objectToSend)
        {
            Debug.Log("objectToSend: " + objectToSend);
            return JsonConvert.SerializeObject(new
            {
                id = "Unity_client", type = type,
                data = objectToSend,
                current_intent_name = IntentContext.Instance.Name
            });
        }

        public static string CreateJsonObjectWithoutData(string type)
        {
            return JsonConvert.SerializeObject(new
            {
                id = "Unity_client", type = type,
                current_intent_name = IntentContext.Instance.Name
            });
        }
    }
}
