﻿using UnityEngine;
using UnityEngine.UI;
using UnityToPython;
using Utilities;

namespace Objects.Buttons
{
    public class FinishedButtonScript : MonoBehaviour
    {
        public static Button Button { get; private set; }

        private void Start()
        {
            Button = gameObject.GetComponent<Button>();
            Button.onClick.AddListener(OnClick);
        }

        private void OnClick()
        {
            if (RtcServer.Instance.Signaler.PeerConnection.IsConnected)
            {
                UnityToPythonDataChannel.Instance.SendMessage(
                    JsonHelper.CreateJsonObjectWithData("input_form", ToggleEmotionsScript.GetSelectedEmotion().name));
                ToggleEmotionsScript.ToggleEmotions.SetAllTogglesOff();
            }
        }

        private void Update()
        {
        }
    }
}
