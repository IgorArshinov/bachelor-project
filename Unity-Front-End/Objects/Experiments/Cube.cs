﻿using Newtonsoft.Json;
using Strategies;
using Strategies.Intents;
using UnityEngine;
using UnityToPython;

namespace Objects
{
    public class Cube : MonoBehaviour
    {
        
        private RtcServer _rtcServer;

        void Start()
        {
            _rtcServer = FindObjectOfType(typeof(RtcServer)) as RtcServer;
        }

        
        void Update()
        {
        }

        void OnMouseDown()
        {
            Debug.Log("OnMouseDown: " + this.name);
            if (_rtcServer.Signaler.PeerConnection.IsConnected)
            {
                var jsonObject =
                    JsonConvert.SerializeObject(new {type = "stop", current_intent_name = IntentContext.Instance.Name});

                UnityToPythonDataChannel.Instance.SendMessage(jsonObject);
            }
        }
    }
}
