﻿using UnityEngine;
using UnityEngine.UI;

namespace Objects.Buttons
{
    public class MenuButtonScript : MonoBehaviour
    {
        private static Button Button { get; set; }

        private void Awake()
        {
            Button = gameObject.GetComponent<Button>();
            Button.onClick.AddListener(OnClick);
        }

        private void OnClick()
        {
            GameObjectManagerScript.GameObjects["popUpContainer"].SetActive(true);
            GameObjectManagerScript.GameObjects["menuPanel"].SetActive(true);
        }

        private void Start()
        {
        }

        private void Update()
        {
        }
    }
}
