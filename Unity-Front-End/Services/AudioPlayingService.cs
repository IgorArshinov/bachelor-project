﻿using System;
using System.IO;
using NAudio.Wave;
using Objects;
using Utilities;

namespace Services
{
    public sealed class AudioPlayingService
    {
        private static AudioPlayingService instance;
        private byte[] _audioFile = Array.Empty<byte>();

        private string PathToAudioFileFromPython { get; set; }

        private AudioPlayingService()
        {
            MainThreadDispatcherScript.Instance().Enqueue(() =>
            {
                PathToAudioFileFromPython = DataPathHelper.pathToAudioFileFromPython;
            });
        }

        public static AudioPlayingService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AudioPlayingService();
                }

                return instance;
            }
        }

        public void CollectAudioDataBytes(byte[] data)
        {
            _audioFile = ArrayHelper.ConcatArrays(_audioFile, data);
        }

        public void WriteAudioDataBytesToWaveFile(string pathToAudioFile)
        {
            WriteAudioDataBytes(pathToAudioFile);
        }

        private void WriteAudioDataBytes(string pathToAudioFile)
        {
            using (var rawSourceWaveStream =
                new RawSourceWaveStream(new MemoryStream(_audioFile), new WaveFormat(24000, 16, 1)))
            {
                WaveFileWriter.CreateWaveFile(
                    pathToAudioFile, rawSourceWaveStream);
            }
        }

        public void WriteAudioDataBytesFromPythonToWaveFile()
        {
            WriteAudioDataBytes(PathToAudioFileFromPython);
        }

        public void ClearAudioData()
        {
            _audioFile = Array.Empty<byte>();
        }

        public async void PlayAudioFileFromPython()
        {
            await DialogflowAudioSourceScript.LoadClip(PathToAudioFileFromPython);
            DialogflowAudioSourceScript.PlayAudioSource();
        }
    }
}
