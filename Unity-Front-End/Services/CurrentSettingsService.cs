﻿using System.Collections.Generic;
using Microsoft.MixedReality.WebRTC;
using Models;
using Models.Interfaces;
using UnityEngine;

namespace Services
{
    public class CurrentSettingsService
    {
        private static CurrentSettingsService instance;
        public readonly Dictionary<string, ISetting> CurrentSettings = new Dictionary<string, ISetting>();

        public static CurrentSettingsService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CurrentSettingsService();
                }

                return instance;
            }
        }
    }
}
