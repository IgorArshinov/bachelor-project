﻿using System;
using Enums;
using Objects;
using UnityEngine;
using UnityToPython;

namespace Utilities
{
    public static class GUIHelper
    {
        public static void AddTextToChatbotTextPanel(string text)
        {
            Debug.Log("text: " + text);
            ChatbotFulfillmentTextScript.ChatbotFulfillmentText.text =
                ChatbotFulfillmentTextScript.ChatbotFulfillmentText.text +
                Environment.NewLine + Environment.NewLine + text;
        }

        public static void SetAllPanelsToInactive()
        {
            foreach (Transform child in PanelsScript.Panels.transform)
            {
                child.gameObject.SetActive(false);
            }
        }

        public static void SetPanelToActive(string nameWithUnderscores)
        {
            var nameWithOutUnderscores = nameWithUnderscores.Replace("_", "");
            foreach (Transform child in PanelsScript.Panels.transform)
            {
                child.gameObject.SetActive(false);
                if (child.gameObject.name.Equals(nameWithOutUnderscores))
                {
                    child.gameObject.SetActive(true);
                }
            }
        }
    }
}
