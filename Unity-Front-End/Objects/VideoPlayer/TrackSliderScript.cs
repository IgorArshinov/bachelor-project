﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Video;

public class TrackSliderScript : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] private VideoPlayer videoPlayer;
    public static Slider TrackSlider { get; private set; }
    private bool _sliderIsUsed;

    void Start()
    {
    }


    void Update()
    {
        if (!_sliderIsUsed)
        {
            TrackSlider.value = videoPlayer.frame / (float) videoPlayer.frameCount;
        }
    }

    private void Awake()
    {
        TrackSlider = gameObject.GetComponent<Slider>();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        _sliderIsUsed = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        float frame = TrackSlider.value * videoPlayer.frameCount;
        videoPlayer.frame = (long) frame;
        _sliderIsUsed = false;
    }
}
