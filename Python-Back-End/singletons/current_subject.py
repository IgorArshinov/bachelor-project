import math
import os

from constants.paths import COLLECTED_DATA_PATH
from enums.intent_enum import Intents
from singletons.logger import Logger


class CurrentSubject:
    __instance = None

    @staticmethod
    def get_instance():
        if CurrentSubject.__instance is None:
            CurrentSubject()
        return CurrentSubject.__instance

    def __init__(self):
        self.name = "current"
        self.surname = "subject"
        self.__full_name = self.name + "_" + self.surname
        self.age = None
        self.gender = None
        self.native_language = None
        self.session_number = "session_1"
        self.current_task_emotion = ""
        self.audios = []
        self.audios_from_tasks = []
        self.forms = self.forms = {}
        self.tasks = {"attentiveness": False, "fatigue": False, "joviality": False, "sadness": False}
        self.distances = {}
        self.current_valence = 0
        self.current_arousal = 0
        self.current_label_for_emotion = None
        self.current_intent_name = None
        self.chatbot_waits_for_audio_input = False
        self.current_task_file_number = 0
        self.is_doing_tasks = False
        self.is_watching_video = False
        self.current_saving_path_for_audio = self.get_path_to_subject_dialogflow_audio_directory()
        self.current_saving_path_for_video = self.get_path_to_subject_dialogflow_video_directory()
        if CurrentSubject.__instance is not None:
            raise Exception("This class is a singleton!")
        else:
            CurrentSubject.__instance = self

    @property
    def full_name(self):
        return self.name + "_" + self.surname

    @full_name.setter
    def full_name(self, full_name):
        self.__full_name = full_name

    def set_current_task_as_finished(self):
        self.tasks[self.current_task_emotion] = True

    def reset_label_and_affective_values(self):
        self.current_valence = None
        self.current_arousal = None
        self.current_label_for_emotion = None

    def calculate_distances(self):
        for emotion_label, values in self.forms["affective_values_for_emotions"].items():
            self.distances[emotion_label] = math.hypot(values["arousal_value"] - self.current_arousal,
                                                       values["valence_value"] - self.current_valence)
        Logger.get_instance().logger.info(self.distances)
        Logger.get_instance().logger.info(min(self.distances, key=self.distances.get))

    def get_next_task_intent_for_subject(self):
        self.current_task_file_number = 0
        next_tasks = {}
        for emotion_label, task_is_done in self.tasks.items():
            if not task_is_done:
                next_tasks[emotion_label] = math.hypot(
                    self.forms["affective_values_for_emotions"][emotion_label]["arousal_value"] - self.current_arousal,
                    self.forms["affective_values_for_emotions"][emotion_label]["valence_value"] - self.current_valence)

        if len(next_tasks) is 0:
            self.current_task_emotion = None
            return None
        self.current_task_emotion = min(next_tasks, key=next_tasks.get)
        self.audios_from_tasks.append({
            "id": CurrentSubject.get_instance().current_task_emotion,
            "audios": []
        })

        return Intents["TASK_QUESTION_" + self.current_task_emotion.upper()]

    def get_path_to_subject_json_data_directory(self):
        path = COLLECTED_DATA_PATH + self.full_name + "/" + self.session_number + "/json_data/"
        if not os.path.exists(path):
            os.makedirs(path)
            os.chmod(path, 0o777)
        return path

    def get_path_to_subject_dialogflow_audio_directory(self):
        path = COLLECTED_DATA_PATH + self.full_name + "/" + self.session_number + "/audios/dialogflow/"
        if not os.path.exists(path):
            os.makedirs(path)
            os.chmod(path, 0o777)
        return path

    def get_path_to_subject_dialogflow_video_directory(self):
        path = COLLECTED_DATA_PATH + self.full_name + "/" + self.session_number + "/videos/dialogflow/"
        if not os.path.exists(path):
            os.makedirs(path)
            os.chmod(path, 0o777)
        return path

    def get_path_to_subject_dialogflow_audio_task_directory(self):
        path = COLLECTED_DATA_PATH + self.full_name + "/" + self.session_number + "/audios/dialogflow/" + self.current_task_emotion + "/"
        if not os.path.exists(path):
            os.makedirs(path)
            os.chmod(path, 0o777)
        return path

    def get_path_to_subject_dialogflow_video_task_directory(self):
        path = COLLECTED_DATA_PATH + self.full_name + "/" + self.session_number + "/videos/dialogflow/" + self.current_task_emotion + "/"
        if not os.path.exists(path):
            os.makedirs(path)
            os.chmod(path, 0o777)
        return path
