﻿namespace Strategies.Intents.Interfaces
{
    public interface IIntentStrategy
    {
        void MakeChangesInUnity(string fulfillmentText);
    }
}
