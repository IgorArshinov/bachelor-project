﻿using TMPro;
using UnityEngine;

namespace Objects
{
    public class ChatbotFulfillmentTextScript : MonoBehaviour
    {
        public static TMP_Text ChatbotFulfillmentText { get; private set; }

        void Start()
        {
            ChatbotFulfillmentText = gameObject.GetComponent<TMP_Text>();
        }
        
        void Update()
        {
        }
    }
}
