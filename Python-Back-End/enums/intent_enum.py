from enum import Enum


class Intents(Enum):
    INTRODUCTION = {
        "intent_name": "Introduction",
        "chatbot_waits_for_audio_input": False,
    }
    SMALL_TALK_ABOUT_LANGUAGE = {
        "intent_name": "Small_Talk_About_Language",
        "chatbot_waits_for_audio_input": True,
    }
    DESCRIBE_YOUR_EMOTION_IN_ONE_WORD = {
        "intent_name": "Describe_Your_Emotion_In_One_Word",
        "chatbot_waits_for_audio_input": False,
    }
    SELECT_AFFECTIVE_SLIDER_VALUES = {
        "intent_name": "Select_Affective_Slider_Values",
        "chatbot_waits_for_audio_input": False,
    }
    SMALL_TALK_ABOUT_EMOTIONS_AND_QUESTION_ABOUT_ROBOT_EMOTIONS = {
        "intent_name": "Small_Talk_About_Emotions_And_Question_About_Robot_Emotions",
        "chatbot_waits_for_audio_input": True,
    }
    SMALL_TALK_ABOUT_FAVORITE_ARTISTS = {
        "intent_name": "Small_Talk_About_Favorite_Artists",
        "chatbot_waits_for_audio_input": True,
    }
    FORM_FOR_METADATA = {
        "intent_name": "Form_For_Metadata",
        "chatbot_waits_for_audio_input": False,
    }
    FORM_FOR_AFFECTIVE_VALUES_FOR_EMOTIONS = {
        "intent_name": "Form_For_Affective_Values_For_Emotions",
        "chatbot_waits_for_audio_input": False,
    }
    EXPLANATION_ABOUT_TASKS = {
        "intent_name": "Explanation_About_Tasks",
        "chatbot_waits_for_audio_input": False,
    }
    BAD_INPUT_FALLBACK = {
        "intent_name": "Bad_Input_Fallback",
        "chatbot_waits_for_audio_input": True,
    }
    TASK_QUESTION_SADNESS = {
        "intent_name": "Task_Question_Sadness",
        "chatbot_waits_for_audio_input": True,
    }
    TASK_QUESTION_FATIGUE = {
        "intent_name": "Task_Question_Fatigue",
        "chatbot_waits_for_audio_input": True,
    }
    TASK_QUESTION_JOVIALITY = {
        "intent_name": "Task_Question_Joviality",
        "chatbot_waits_for_audio_input": True,
    }
    TASK_QUESTION_ATTENTIVENESS = {
        "intent_name": "Task_Question_Attentiveness",
        "chatbot_waits_for_audio_input": True,
    }
    DESCRIBE_YOUR_EMOTION_IN_MULTIPLE_SENTENCES_AFTER_TASK = {
        "intent_name": "Describe_Your_Emotion_In_Multiple_Sentences_After_Task",
        "chatbot_waits_for_audio_input": True,
    }
    DESCRIBE_YOUR_EMOTION_IN_ONE_WORD_AFTER_TASK = {
        "intent_name": "Describe_Your_Emotion_In_One_Word_After_Task",
        "chatbot_waits_for_audio_input": False,
    }
    END_SESSION = {
        "intent_name": "End_Session",
        "chatbot_waits_for_audio_input": False,
    }
    SELECT_AFFECTIVE_SLIDER_VALUES_AFTER_TASK = {
        "intent_name": "Select_Affective_Slider_Values_After_Task",
        "chatbot_waits_for_audio_input": False,
    }
    DIALOGFLOW_DID_NOT_GOT_PARAMETER = {
        "chatbot_waits_for_audio_input": False,
    }

    @classmethod
    def get_value_by_intent_number(self, number):
        for intent in self:
            if intent.value["intent_number"] == number:
                return intent
