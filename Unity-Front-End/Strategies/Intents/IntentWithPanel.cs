﻿using Objects;
using Strategies.Intents.Interfaces;
using Utilities;

namespace Strategies.Intents
{
    public class IntentWithPanel : IIntentStrategy
    {
        private string IntentName { get; set; }

        public IntentWithPanel(string intentName)
        {
            IntentName = intentName;
        }

        public void MakeChangesInUnity(string fulfillmentText)
        {
            GUIHelper.SetAllPanelsToInactive();
            GUIHelper.SetPanelToActive(IntentName);
            GUIHelper.AddTextToChatbotTextPanel(fulfillmentText);
        }
    }
}
