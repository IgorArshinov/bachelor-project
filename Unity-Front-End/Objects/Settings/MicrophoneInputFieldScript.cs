﻿using TMPro;
using UnityEngine;

namespace Objects
{
    public class MicrophoneInputFieldScript : MonoBehaviour
    {
        private TMP_InputField _inputField;

        private void Awake()
        {
            _inputField = GetComponent<TMP_InputField>();
            _inputField.text = Microphone.devices[0];
        }

        private void Start()
        {
        }

        private void Update()
        {
        }
    }
}
