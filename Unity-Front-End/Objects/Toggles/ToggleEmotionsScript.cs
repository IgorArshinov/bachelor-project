﻿using System;
using System.Linq;
using Objects.Buttons;
using UnityEngine;
using UnityEngine.UI;

namespace Objects
{
    public class ToggleEmotionsScript : MonoBehaviour
    {
        public static ToggleGroup ToggleEmotions { get; private set; }
        public static Toggle[] ToggleEmotionsChildren { get; private set; }

        private void Awake()
        {
            ToggleEmotions = gameObject.GetComponent<ToggleGroup>();
            ToggleEmotionsChildren = gameObject.GetComponentsInChildren<Toggle>();
            ToggleEmotions.SetAllTogglesOff();
            foreach (var toggleEmotion in ToggleEmotionsChildren)
            {
                toggleEmotion.onValueChanged.AddListener((bool isOn) =>
                {
                    if (isOn)
                    {
                        FinishedButtonScript.Button.interactable = true;
                    }
                    else if (!GetSelectedEmotion())
                    {
                        FinishedButtonScript.Button.interactable = false;
                    }
                });
            }
        }

        void Start()
        {
        }
        
        void Update()
        {
        }

        public static Toggle GetSelectedEmotion()
        {
            return ToggleEmotions.ActiveToggles().FirstOrDefault();
        }
    }
}
