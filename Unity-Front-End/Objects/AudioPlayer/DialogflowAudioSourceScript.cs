﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Strategies.Intents;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityToPython;
using Utilities;

namespace Objects
{
    public class DialogflowAudioSourceScript : MonoBehaviour
    {
        private static AudioSource audioSource;
        public UnityEvent onFinishSound;
        private float _clipDuration;
        private static bool chatbotIsTalking;

        void Start()
        {
        }

        void Awake()
        {
            audioSource = gameObject.GetComponent<AudioSource>();
        }

        void Update()
        {
            if (!audioSource.isPlaying && chatbotIsTalking)
            {
                if (IntentContext.Instance.ChatbotWaitsForAudioInput)
                {
                    if (RtcServer.Instance.Signaler.PeerConnection.IsConnected)
                    {
                        UnityToPythonDataChannel.Instance.SendMessage(
                            JsonHelper.CreateJsonObjectWithoutData("chatbot_waits_for_audio_input"));
                    }
                }

                chatbotIsTalking = false;
            }
        }

        public static void PlayAudioSource()
        {
            audioSource.Play();
            chatbotIsTalking = true;
        }

        public static async Task LoadClip(string path)
        {
            using (UnityWebRequest unityWebRequest = UnityWebRequestMultimedia.GetAudioClip(path, AudioType.WAV))
            {
                unityWebRequest.SendWebRequest();

                try
                {
                    while (!unityWebRequest.isDone) await Task.Delay(5);

                    if (unityWebRequest.isNetworkError || unityWebRequest.isHttpError)
                        Debug.Log($"{unityWebRequest.error}");
                    else
                    {
                        audioSource.clip = DownloadHandlerAudioClip.GetContent(unityWebRequest);
                    }
                }
                catch (Exception err)
                {
                    Debug.Log($"{err.Message}, {err.StackTrace}");
                }
            }
        }
    }
}
