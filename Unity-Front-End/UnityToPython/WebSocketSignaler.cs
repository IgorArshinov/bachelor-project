using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;
using WebSocketSharp.Server;

namespace UnityToPython
{
    public class WebSocketSignaler : Signaler
    {
        // pfx file
        private int _serverPort;
        private WebSocketServer _server;
        private WebRtcSession _session;

        public WebSocketSignaler(int port)
        {
            _serverPort = port;
        }

        public override void Start()
        {
            _server = new WebSocketServer(IPAddress.Any, _serverPort, true);
            var path = System.IO.Path.Combine(Application.streamingAssetsPath, "Certs", "localhost.pfx.bin");
#if UNITY_WINRT
            byte[] content = UnityEngine.Windows.File.ReadAllBytes(path);
#else
            byte[] content = System.IO.File.ReadAllBytes(path);
#endif
            _server.SslConfiguration.ServerCertificate =
                new System.Security.Cryptography.X509Certificates.X509Certificate2(content);
            _server.SslConfiguration.CheckCertificateRevocation = false;
            _server.AddWebSocketService<WebRtcSession>("/", newSession =>
            {
                Debug.Log("Incoming connection ...");
                if (_session != null)
                {
                    Debug.Log("Another session is running. Ignore request");
                    return;
                }

                newSession.messageReceived += MessageReceived;
                newSession.socketOpen += () =>
                {
                    Debug.Log("Socket open!");
                    ClientConnected?.Invoke();
                };
                newSession.socketClosed += () =>
                {
                    Debug.Log("Socket closed!");
                    _session = null;
                };
                newSession.connectionError += (errorMessage) =>
                {
                    Debug.LogWarning(errorMessage);
                    newSession.Context.WebSocket.Close();
                    // OnClose is only triggered when the connection was established before
                    // unsetting _session in case SocketClosed was not fired. 
                    _session = null;
                };
                _session = newSession;
            });
            _server.Start();
            Debug.Log($"Waiting for browser web socket connection to {_server.Address}:{_server.Port}...");
        }

        public override void SendMessage(JObject json)
        {
            _session.Context.WebSocket.Send(JsonConvert.SerializeObject(json, Formatting.None));
        }

        private void MessageReceived(string msg)
        {
            JObject json = JObject.Parse(msg);
            ProcessIncomingMessage(json);
        }

        public override void Stop()
        {
            _server.Stop();
        }
    }
}
