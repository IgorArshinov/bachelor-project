import json


def write_to_json_file(data, path):
    with open(path,
              'w') as file:
        json.dump(data, file, indent=4)
