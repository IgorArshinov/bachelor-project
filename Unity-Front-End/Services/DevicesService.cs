﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.MixedReality.WebRTC;
using UnityEngine;

namespace Services
{
    public class DevicesService
    {
        private DevicesService()
        {
        }

        private static DevicesService instance;

        private readonly IReadOnlyList<VideoCaptureDevice> _deviceList =
            DeviceVideoTrackSource.GetCaptureDevicesAsync().Result;

        public static DevicesService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DevicesService();
                }

                return instance;
            }
        }

        public List<string> GetAllWebcamNames()
        {
            return _deviceList.Select(d => d.name).ToList();
        }

        public VideoCaptureDevice GetVideoDeviceByName(string name)
        {
            return _deviceList.First(d => d.name.Equals(name));
        }

        public async Task<List<string>> GetResolutionsForWebcamByName(string name)
        {
            Debug.Log("_deviceList: " + _deviceList.Count);
            var device = _deviceList.First(d => d.name.Equals(name));
            var profiles = await DeviceVideoTrackSource.GetCaptureProfilesAsync(device.id);

            var resolutions = new List<string>();
            if (profiles.Count > 0)
            {
                foreach (var profile in profiles)
                {
                    var configs =
                        await DeviceVideoTrackSource.GetCaptureFormatsAsync(device.id, profile.uniqueId);
                    foreach (var config in configs)
                    {
                        resolutions.Add(config.width + "x" + config.height + ":" + config.framerate +
                                        "FPS");
                    }
                }
            }
            else
            {
                var configs = await DeviceVideoTrackSource.GetCaptureFormatsAsync(device.id);
                foreach (var config in configs)
                {
                    resolutions.Add(config.width + "x" + config.height + ":" + config.framerate +
                                    "FPS");
                }
            }

            return resolutions;
        }
    }
}
