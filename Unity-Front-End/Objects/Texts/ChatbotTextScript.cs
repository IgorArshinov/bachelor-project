﻿using UnityEngine;

namespace Objects
{
    public class ChatbotTextScript : MonoBehaviour
    {
        public static GameObject ChatbotText { get; private set; }

        private void Awake()
        {
            ChatbotText = gameObject;
        }

        private void Start()
        {
        }

        private void Update()
        {
        }
    }
}
