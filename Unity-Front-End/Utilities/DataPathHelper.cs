﻿using System.IO;
using UnityEngine;

namespace Utilities
{
    public static class DataPathHelper
    {
        public static string pathToAudioFileFromPython =
            Path.Combine(Application.dataPath, "Resources", "file.wav");
    }
}
