﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using Models;
using Services;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityToPython;

namespace Objects.Buttons
{
    public class StartButtonScript : MonoBehaviour
    {
        public static Button Button { get; private set; }
        [SerializeField] private GameObject settings;

        private void Awake()
        {
            Button = gameObject.GetComponent<Button>();
            Button.onClick.AddListener(OnClick);
        }

        private void OnClick()
        {
            var microphoneName = settings.transform.Find("MicrophoneField").GetComponentInChildren<TMP_InputField>()
                .text;
            var dropdownForWebcams = settings.transform.Find("DropdownFieldForWebcams").transform
                .Find("DropdownForWebcams")
                .GetComponentInChildren<TMP_Dropdown>();
            var dropdownForWebcamResolutions = settings.transform.Find("DropdownFieldForWebcams")
                .Find("DropdownForWebcamResolutions")
                .GetComponentInChildren<TMP_Dropdown>();


            var webcamName = dropdownForWebcams.options[dropdownForWebcams.value].text;
            if (!webcamName.Equals("Geen"))
            {
                var formatString = dropdownForWebcamResolutions.options[dropdownForWebcamResolutions.value].text.Trim();
                string[] stringWithNumbers = Regex.Split(formatString, @"\D+");

                int[] integers = Array.ConvertAll(stringWithNumbers.Where(n => !string.IsNullOrEmpty(n)).ToArray(),
                    s => int.Parse(s));
                var videoDevice = DevicesService.Instance.GetVideoDeviceByName(webcamName);
                Debug.Log("videoDevice: " + videoDevice.id);
                CurrentSettingsService.Instance.CurrentSettings["videoDevice"] = new VideoDevice()
                {
                    ID = videoDevice.id,
                    Name = videoDevice.name,
                    Width = integers[0],
                    Height = integers[1],
                    FramesPerSecond = integers[2],
                    VideoCaptureDevice = videoDevice
                };


                RtcServer.Instance.needVideo = true;
            }
            else
            {
                RtcServer.Instance.needVideo = false;
            }

            RtcServer.Instance.Signaler.Start();
             
            GameObjectManagerScript.GameObjects["loadingWhenConnecting"].SetActive(true);
            Button.interactable = false;
            DropdownForWebcamsScript.Dropdown.interactable = false;
        }

        private void Start()
        {
        }

        private void Update()
        {
        }
    }
}
