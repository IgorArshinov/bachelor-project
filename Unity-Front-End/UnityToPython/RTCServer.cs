﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.MixedReality.WebRTC;
using Models;
using Services;
using UnityEngine;
using AudioTrackSource = Microsoft.MixedReality.WebRTC.AudioTrackSource;
using VideoTrackSource = Microsoft.MixedReality.WebRTC.VideoTrackSource;

namespace UnityToPython
{
    public class RtcServer : MonoBehaviour
    {
        public Signaler Signaler;
        private Transceiver _audioTransceiver;
        private Transceiver _videoTransceiver;
        private AudioTrackSource _audioTrackSource;
        private VideoTrackSource _videoTrackSource;
        private LocalAudioTrack _localAudioTrack;
        private LocalVideoTrack _localVideoTrack;

        IReadOnlyList<VideoCaptureDevice> _deviceList;
        public VideoCaptureDevice webcamDevice;

        public bool needVideo = true;
        public bool needAudio = true;

        public uint videoWidth = 640;
        public uint videoHeight = 480;
        public uint videoFps = 30;
        public string videoProfileId = "";

        public int port = 9999;
        public SignalerType connectionType = SignalerType.Tcp;

        public bool useRemoteStun = false;
        public static RtcServer Instance { get; private set; }

        async void Start()
        {
            Instance = this;
            _deviceList = await DeviceVideoTrackSource.GetCaptureDevicesAsync();
            Debug.Log($"Found {_deviceList.Count} devices.");
            foreach (var device in _deviceList)
            {
                Debug.Log($"Found webcam {device.name} (id: {device.id}):");
                var profiles = await DeviceVideoTrackSource.GetCaptureProfilesAsync(device.id);
                if (profiles.Count > 0)
                {
                    foreach (var profile in profiles)
                    {
                        Debug.Log($"+ Profile '{profile.uniqueId}'");
                        var configs = await DeviceVideoTrackSource.GetCaptureFormatsAsync(device.id, profile.uniqueId);
                        foreach (var config in configs)
                        {
                            Debug.Log($"  - {config.width}x{config.height}@{config.framerate}");
                        }
                    }
                }
                else
                {
                    var configs = await DeviceVideoTrackSource.GetCaptureFormatsAsync(device.id);
                    foreach (var config in configs)
                    {
                        Debug.Log($"- {config.width}x{config.height}@{config.framerate}");
                    }
                }
            }

            switch (connectionType)
            {
                case SignalerType.Tcp:
                    Signaler = new TcpSignaler(port);
                    break;
                case SignalerType.WebSocket:
                    Signaler = new WebSocketSignaler(port);
                    break;
                default:
                    throw new System.Exception($"Signaler connection type {connectionType} is not valid!");
            }


            Signaler.ClientConnected += OnClientConnected;
            Signaler.ClientDisconnected += OnClientDisconnected;
            if (useRemoteStun)
            {
                Signaler.IceServers.Add(new IceServer {Urls = {"stun:stun.l.google.com:19302"}});
            }

             
        }

        async void OnClientConnected()
        {
            var pc = Signaler.PeerConnection;

            foreach (var channel in pc.DataChannels)
            {
                Debug.Log("channel: " + channel.Label);
            }

            if (needVideo)
            {
                if (CurrentSettingsService.Instance.CurrentSettings["videoDevice"] is VideoDevice videoDevice)
                {
                    var deviceSettings = new LocalVideoDeviceInitConfig
                    {
                        width = Convert.ToUInt32(videoDevice.Width),
                        height = Convert.ToUInt32(videoDevice.Height),
                        framerate = videoDevice.FramesPerSecond,
                        videoProfileId = videoDevice.ID,
                        videoDevice = videoDevice.VideoCaptureDevice
                    };


                    try
                    {
                        Debug.Log(
                            $"Attempt to grab Camera - {deviceSettings.videoProfileId}: {deviceSettings.width}x{deviceSettings.height}@{deviceSettings.framerate}fps");
                        _videoTrackSource = await DeviceVideoTrackSource.CreateAsync(deviceSettings);
                    }
                    catch (Exception ex)
                    {
                        Debug.LogException(ex, this);
                    }
                }

                 

                Debug.Log($"Create local video track... {_videoTrackSource}");
                var trackSettings = new LocalVideoTrackInitConfig
                {
                    trackName = "webcam_track"
                };
                _localVideoTrack = LocalVideoTrack.CreateFromSource(_videoTrackSource, trackSettings);

                Debug.Log("Create video transceiver and add webcam track...");
                _videoTransceiver = pc.AddTransceiver(MediaKind.Video);
                _videoTransceiver.DesiredDirection = Transceiver.Direction.SendReceive;
                 
                _videoTransceiver.LocalVideoTrack = _localVideoTrack;
            }

             
            if (needAudio)
            {
                Debug.Log("Opening local microphone...");

                var initConfig = new LocalAudioDeviceInitConfig
                {
                    AutoGainControl = true,
                };
                try
                {
                    _audioTrackSource = await DeviceAudioTrackSource.CreateAsync(initConfig);
                }
                catch (Exception ex)
                {
                    Debug.LogException(ex, this);
                }

                Debug.Log("Create local audio track...");
                var trackSettings = new LocalAudioTrackInitConfig {trackName = "mic_track"};
                _localAudioTrack = LocalAudioTrack.CreateFromSource(_audioTrackSource, trackSettings);

                Debug.Log("Create audio transceiver and add mic track...");
                _audioTransceiver = pc.AddTransceiver(MediaKind.Audio);
                _audioTransceiver.DesiredDirection = Transceiver.Direction.SendReceive;
                _audioTransceiver.LocalAudioTrack = _localAudioTrack;
            }

             
            int numFrames = 0;
            pc.VideoTrackAdded += (RemoteVideoTrack track) =>
            {
                Debug.Log($"Attach Frame Listener...");
                track.I420AVideoFrameReady += (I420AVideoFrame frame) =>
                {
                    ++numFrames;
                     
                    if (numFrames % 60 == 0)
                    {
                        Debug.Log($"Received video frames: {numFrames}");
                    }
                };
            };

            pc.CreateOffer();

            Debug.Log("Send offer to remote peer");
        }

        public void OnClientDisconnected()
        {
            _localAudioTrack?.Dispose();
            _localVideoTrack?.Dispose();
            _audioTrackSource?.Dispose();
            _videoTrackSource?.Dispose();
        }

        public void OnDisable()
        {
            OnClientDisconnected();
            Signaler?.Stop();

            Debug.Log("Program terminated.");
        }

        public enum SignalerType
        {
            Tcp = 0,
            WebSocket = 1
        }
    }
}
